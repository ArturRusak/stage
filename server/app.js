'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const db = require('./dao/db');
const auth = require('./routes/auth');
const adverts = require('./routes/adverts');
const users = require('./routes/users');

const PORT = 3001;

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

/**
 * Routes
 */
app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, '../public/index.html'));
});
app.use('/', auth);
app.use('/api/users', users);
app.use('/api/adverts', adverts);

(async () => {
  db.connect();
  await app.listen(PORT, () => {
    console.log(`App listening on ${PORT}!`)
  });
})();

module.exports = app;
