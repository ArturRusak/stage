const config = {
  db: {
    connectionLimit : 10,
    host: 'localhost',
    user: 'root',
    password: 'root',
    database : 'adverts_board'
  },
  jwt: {
    secretKey: 'TIXO',
    tokens: {
      access: {
        type: 'access',
        expiresIn: '2m'
      }
    }
  },
  bcrypt: {
    rounds: 10
  }

};

module.exports = config;