const Joi = require('joi');

const validateCreateUser = (req, res, next) => {
  validateData(req, res, next, createUserSchema);
};

const validateUpdateUser = (req, res, next) => {
  validateData(req, res, next, updateUserSchema);
};

const validateCreateAdvert = (req, res, next) => {
  validateData(req, res, next, createAdvertSchema);
};

const validateUpdateAdvert = (req, res, next) => {
  validateData(req, res, next, updateAdvertSchema);
};

const validateLogin = (req, res, next) => {
  validateData(req, res, next, loginSchema);
};

const validateData = (req, res, next, schema) => {
  Joi.validate(req.body, schema)
    .then(() => next())
    .catch(validation =>{
      const errorsList = [];
      validation.details.map((error) => {
        errorsList.push(error.message);
      });
      res
        .status(400)
        .send({ errors: errorsList})
    });
};

const createUserSchema = Joi.object().keys({
  userName: Joi.string().min(4).max(20).regex(/^[A-Za-z\.0-9\-]/).required().error(() => 'User Name is require'),
  firstName: Joi.string().min(2).max(20).regex(/^[А-Яа-яA-Za-z\ \-]/).required().error(() => 'First Name is require'),
  lastName: Joi.string().min(2).max(20).regex(/^[А-Яа-яA-Za-z]/).required().error(() => 'Last Name is require'),
  email: Joi.string().min(3).max(30).email().required().error(() => 'Email is require'),
  password: Joi.string().required(),
  phone: Joi.string().min(13).max(13).regex(/^\+375\d{9}$/).error(() => 'Phone is require')
});

const loginSchema = Joi.object().keys({
  userName: Joi.string().required().error(() => 'User Name is require'),
  password: Joi.string().required().error(() => 'Password is require')
});

const createAdvertSchema = Joi.object().keys({
  title: Joi.string().min(10).max(50).regex(/^[A-Za-z\.0-9\-]/).required().error(() => 'Title is require'),
  description: Joi.string().required().regex(/^[A-Za-z\.0-9\-]/).error(() => 'Description is require'),
  category: Joi.string().required().regex(/^[A-Za-z\.0-9\-]/).error(() => 'Category is require'),
  price: Joi.number().positive().required().error(() => 'Price is require'),
  user_id: Joi.number().required()
});

const updateUserSchema = Joi.object().keys({
  id: Joi.number().required().error(() => 'ID must be a number'),
  userName: Joi.string().min(4).max(20).regex(/^[A-Za-z\.0-9\-]/).required().error(() => 'User Name is require'),
  firstName: Joi.string().min(2).max(20).regex(/^[А-Яа-яA-Za-z\ \-]/).required().error(() => 'First Name is require'),
  lastName: Joi.string().min(2).max(20).regex(/^[А-Яа-яA-Za-z]/).required().error(() => 'Last Name is require'),
  email: Joi.string().min(3).max(30).email().required().error(() => 'Email is require'),
  phone: Joi.string().min(13).max(13).regex(/^\+375\d{9}$/).required().error(() => 'Phone is require')
});

const updateAdvertSchema = Joi.object().keys({
  id: Joi.number().required().error(() => 'ID error value'),
  title: Joi.string().min(10).max(50).regex(/^[A-Za-z\.0-9\-]/).required().error(() => 'Title is require'),
  description: Joi.string().required().regex(/^[A-Za-z\.0-9\-]/).error(() => 'Description is require'),
  category: Joi.string().required().regex(/^[A-Za-z\.0-9\-]/).error(() => 'Category is require'),
  price: Joi.number().positive().required().error(() => 'Price is require')
});

module.exports = {
  validateLogin,
  validateCreateUser,
  validateUpdateUser,
  validateCreateAdvert,
  validateUpdateAdvert
};
