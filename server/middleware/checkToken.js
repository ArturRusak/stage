const jwt = require('jsonwebtoken');
const { secretKey } = require('../config').jwt;

const checkToken = (req, res, next) => {
    let token = req.headers['x-access-token'] || req.headers['authorization']; //Express header
    const BEARER = 'Bearer ';

    if (token && token.startsWith(BEARER)) {
        //Remove Bearer from string
        token = token.split(' ')[1];
        jwt.verify(token, secretKey, (err, decoded) => {
           if (err) {
               return res
                 .status(401)
                 .json({
                  success: false,
                  message: 'Token is not valid'
               });
           } else {
               req.decoded = decoded;
               next();
           }
        });
    } else {
        return res
          .status(401)
          .json({
           success: false,
           message: 'Auth token is not supplied!'
        });
    }

};

module.exports = checkToken;