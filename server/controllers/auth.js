'use strict';

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { secretKey, tokens } = require('../config').jwt;
const user = require('../models/users').users;

const signIn = (req, res, next) => {
  const { userName, password } = req.body;

  if (userName && password) {
    user.getUserByName(userName)
      .then((user) => {
        if (!user) {
          res.status(401).json({ message: 'User does not exist!' });
        }

        const isValidPassword = bcrypt.compareSync(password, user[0].password);
        const options = tokens.access.expiresIn;

        if (isValidPassword) {
          let token = jwt.sign({ user }, secretKey, {expiresIn: '30s'});
          // return the JWT token for the future API calls
          res.json({
            message: 'Authentication successful!',
            token: token
          });
          next();
        } else {
          res
            .status(401)
            .json({ message: 'Invalid credentials!' });
        }
      })
      .catch(error => {
        res
          .status(403)
          .send({
            message: error.message
          });
      })
  } else {
    res
      .status(400)
      .send({ message: 'Authentication failed! Please check the request!' });
  }
};

module.exports = {
  signIn
};