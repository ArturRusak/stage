'use strict';

const advert = require('../models/adverts').adverts;

/**
 * Make object to pass in database for update
 * @param {Object} obj - object with
 * @returns {Array} - array of values
 */
function prepareUpdateData(obj) {
  const { title, description, category, price } = obj;
  return [title, description, category, price];
}

/**
 * Make object to pass in database for create
 * @param {Object} obj - object with
 * @returns {Array} - array of values
 */
function prepareCreateData(obj) {
  const { title, description, category, price, user_id } = obj;
  return [title, description, category, price, user_id];
}
/**
 * Create the advert
 * @param {Object} newAdvert - Data of the new advert
 * @returns {Promise}
 */
function createAdvert(newAdvert) {
  return advert.createAdvert(prepareCreateData(newAdvert));
}

/**
 * Get all adverts
 * @returns {Promise}
 */
function getAdverts() {
  return advert.getAdverts();
}

/**
 * Update advert by unique
 * @param {Number} id - Unique of advert
 * @param {Object} newData - New values of the adverts fields
 * @returns {Promise}
 */
function updateAdvert(id, newData) {
  return advert.updateAdvert(id, prepareUpdateData(newData));
}

/**
 * Update advert counter of views by id
 * @param {Number} id - Unique of advert
 * @returns {Promise}
 */
function updateViewsAdvertCounter(id) {
  return advert.updateViewsAdvertCounter(id);
}

/**
 * Get advert by unique
 * @param {Number} id - Unique of advert
 * @returns {Promise}
 */
function getAdvertById(id) {
  return advert.getAdvertById(id);
}

/**
 * Remove advert by unique
 * @param {Number} id - Unique of advert
 * @returns {Promise}
 */
function removeAdvert(id) {
  return advert.removeAdvert(id);
}

module.exports = {
  createAdvert,
  getAdverts,
  getAdvertById,
  removeAdvert,
  updateAdvert,
  updateViewsAdvertCounter
};
