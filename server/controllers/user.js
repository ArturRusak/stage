'use strict';

const bcrypt = require('bcrypt');
const { rounds } = require('../config').bcrypt;
const user = require('../models/users').users;

/**
 * Make object to pass in database with target fields
 * @param {Object} obj - object with
 * @returns {Array} - array of values
 */
// TODO FIX UPDATE
function prepareObjData(obj) {
  const { userName, firstName, lastName, email, phone, password} = obj;
  return [userName, firstName, lastName, email, phone, password];
}

/**
 * Create the user
 * @param {Object} req - Data of the new user
 * @returns {Promise}
 */
function createUser(req) {
  const newUser = req.body;
  const { password } = newUser;

  const salt = bcrypt.genSaltSync(rounds);
  const passwordToSave = bcrypt.hashSync(password, salt);

  return user.createUser(prepareObjData({ ...newUser, password: passwordToSave }));
}

/**
 * Get all users
 * @returns {Promise}
 */
function getUsers() {
  return user.getUsers();
}

/**
 * Update user by unique
 * @param {Number} id - Unique of user
 * @param {Object} newData - New values of the users fields
 * @returns {Promise}
 */
function updateUser(id, newData) {
  return user.updateUser(id, prepareObjData(newData));
}

/**
 * Get user by unique
 * @param {Number} id - Unique of user
 * @returns {Promise}
 */
function getUserById(id) {
  return user.getUserById(id);
}

/**
 * Remove user by unique
 * @param {Number} id - Unique of user
 * @returns {Promise}
 */
function removeUser(id) {
  return user.removeUser(id);
}

module.exports = {
  createUser,
  getUsers,
  getUserById,
  removeUser,
  updateUser
};
