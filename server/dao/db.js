'use strict';

const mysql = require('mysql');
const defaultConfig  = require('../config').db;
const advertsModel = require('../models/adverts');
const usersModel = require('../models/users');
const sql = require('../models/sqlQueries');
let connection;

function connect(config = defaultConfig) {
  connection = mysql.createConnection(config);
  connection.connect((error) => {
    if (error) {
      throw error;
    }
      init();
      //removeTables();
  });
  usersModel.users.setConnection(connection);
  advertsModel.adverts.setConnection(connection);

  return connection;
}

function init() {
  return connection.query(usersModel.createUsersTable, function (err, result) {
    if (err) {
      throw err;
    }
    connection.query(advertsModel.createAdvertTable, function (err, result) {
      if (err) {
        throw err;
      }
      console.log('DB was init!');
    });
  });
}

function removeTables() {
  connection.query(sql.SQL_REMOVE_TABLES, function (err, result) {
    if (err) {
      throw err;
    }
    console.log('DB was removed!');
  });
}

function disconnect() {
  connection.end();
}

function getConnection() {
  return connection;
}

module.exports = {
  connect,
  disconnect,
  getConnection
};
