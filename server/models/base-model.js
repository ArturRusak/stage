class BaseModel {
  setConnection(connection) {
    this.connection = connection;
  }

  query(sql, args) {
    return new Promise((resolve, reject) => {
      this.connection.query(sql, args, (err, rows) => {
        if (err)
          return reject(err);
        resolve(rows);
      });
    });
  }
}

module.exports = BaseModel;
