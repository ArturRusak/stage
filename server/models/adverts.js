'use strict';
const BaseModel = require('./base-model');

const SQL_CREATE_ADVERTS_TABLE = `CREATE TABLE IF NOT EXISTS adverts (` +
  `id int(11) NOT NULL AUTO_INCREMENT, ` +
  `title varchar(50)  NOT NULL, ` +
  `created timestamp  NOT NULL DEFAULT NOW(), ` +
  `modified timestamp NULL, ` +
  `description text  NOT NULL, ` +
  `category text  NOT NULL, ` +
  `price DECIMAL(6,4)  NOT NULL, ` +
  `views int NOT NULL DEFAULT 0, ` +
  `user_id int(11) NOT NULL, ` +
  `PRIMARY KEY  (id), `+
  `FOREIGN KEY (user_id) REFERENCES users (id))`;
const table = 'adverts';
const SQL_CREATE_ADVERT = `INSERT INTO ${table} (title, description, category, price, user_id) VALUES (?)`;
const SQL_GET_ALL_ADVERTS = `SELECT * FROM ${table}`;
const SQL_REMOVE_ADVERT = `DELETE FROM ${table} WHERE id = ?`;
const SQL_GET_ADVERT = `SELECT adverts.*, users.firstName, users.email, users.phone FROM ` +
  `${table} LEFT JOIN users ON adverts.user_id = users.id WHERE adverts.id = ?`;
const SQL_UPDATE_ADVERT = `UPDATE ${table} ` +
  `SET title = ?, description = ?, category = ?, price = ?, modified = NOW() WHERE id = ?`;
const SQL_UPDATE_ADVERT_VIEWS_COUNTER = `UPDATE ${table} SET views = views + 1 WHERE id = ?`;

class Adverts extends BaseModel {

  /**
   * Create new Advert
   * @param {Array} newAdvert values
   * @returns {Promise}
   */
  createAdvert(newAdvert) {
    return this.query(SQL_CREATE_ADVERT, [newAdvert]);
  }

  /**
   * Update advert by id
   * @param {Number} id - Unique id of the advert
   * @param {Array} newData - Values of object fields
   * @returns {Promise}
   */
  updateAdvert(id, newData) {
    const arr = [...newData, id];
    return this.query(SQL_UPDATE_ADVERT, arr);
  }

  /**
   * Update advert counter of views by id
   * @param {Number} id - Unique id of the advert
   * @returns {Promise}
   */
  updateViewsAdvertCounter(id) {
    return this.query(SQL_UPDATE_ADVERT_VIEWS_COUNTER, [id]);
  }

  /**
   * Get all adverts
   * @returns {Promise}
   */
  getAdverts() {
    return this.query(SQL_GET_ALL_ADVERTS)
  }

  /**
   * Remove the advert by id
   * @param {Number} id - Unique id of the advert
   * @returns {Promise}
   */
  getAdvertById(id) {
    return this.query(SQL_GET_ADVERT, [id]);
  }

  /**
   * Get the advert by id
   * @param {Number} id - Unique id of the advert
   * @returns {Promise}
   */
  removeAdvert(id) {
    return this.query(SQL_REMOVE_ADVERT, [id]);
  }

}

module.exports = {
  createAdvertTable: SQL_CREATE_ADVERTS_TABLE,
  adverts: new Adverts()
};
