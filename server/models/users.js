'use strict';

const BaseModel = require('./base-model');

const SQL_CREATE_USERS_TABLE = `CREATE TABLE IF NOT EXISTS users (` +
  `id int(11) NOT NULL AUTO_INCREMENT, ` +
  `userName varchar(20)  NOT NULL, ` +
  `firstName varchar(20)  NOT NULL, ` +
  `lastName varchar(20)  NOT NULL, ` +
  `email varchar(20)  NOT NULL, ` +
  `phone varchar(20)  NOT NULL, ` +
  `password VARCHAR(100)  NOT NULL, ` +
  `UNIQUE (userName), ` +
  `UNIQUE (email), ` +
  `PRIMARY KEY  (id))`;
const table = 'users';
const SQL_CREATE_USER = `INSERT INTO ${table} (userName, firstName, lastName, email, phone, password) VALUES (?)`;
const SQL_GET_ALL_USERS = `SELECT * FROM ${table}`;
const SQL_GET_USER = `SELECT * FROM ${table} WHERE userName = ?`;
const SQL_GET_USER_DATA = `SELECT users.id as us_id, users.userName, ` +
  `users.firstName, users.lastName, users.email, users.phone, adverts.* FROM` +
  ` ${table} LEFT JOIN adverts ON users.id = adverts.user_id WHERE users.id = ?`;
const SQL_REMOVE_USER = `DELETE FROM ${table} WHERE id = ?`;
const SQL_UPDATE_USER = `UPDATE ${table} SET userName = ?, firstName = ?, lastName = ?, ` +
  `email = ?, phone = ? WHERE id = ?`;

class Users extends BaseModel {

  /**
   * Create new user
   * @param {Array} newUser values
   * @returns {Promise}
   */
  createUser(newUser) {
    return this.query(SQL_CREATE_USER, [newUser]);
  }

  /**
   * Update user by id
   * @param {Number} id - Unique id of the user
   * @returns {Promise}
   */
  updateUser(id, newData) {
    const arr = [...newData, id];
    return this.query(SQL_UPDATE_USER, arr);
  }

  /**
   * Get all users
   * @returns {Promise}
   */
  getUsers() {
    return this.query(SQL_GET_ALL_USERS);
  }

  /**
   * Get the user by id
   * @param {Number} id - Unique id of the user
   * @returns {Promise}
   */
  getUserById(id) {
    return this.query(SQL_GET_USER_DATA, [id]);
  }

  /**
   * Get the user by user name
   * @param {Number} userName - Unique id of the user
   * @returns {Promise}
   */
  getUserByName(userName) {
    return this.query(SQL_GET_USER, [userName]);
  }

  /**
   * Get the user by id
   * @param {Number} id - Unique id of the user
   * @returns {Promise}
   */
  removeUser(id) {
    return this.query(SQL_REMOVE_USER, [id])
  }
}

module.exports = {
  createUsersTable: SQL_CREATE_USERS_TABLE,
  users: new Users()
};
