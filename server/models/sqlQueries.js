const usersTable = 'users';
const advertsTable = 'adverts';
const SQL_REMOVE_TABLES = `DROP TABLE IF EXISTS ${advertsTable}, ${usersTable}`;

module.exports = {
  SQL_REMOVE_TABLES
};