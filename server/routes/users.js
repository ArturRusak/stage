'use strict';

const express = require('express');
const user = require('../controllers/user');
const checkToken = require('../middleware/checkToken');
const {
  validateUpdateUser
} = require ('../middleware/validator');


const router = express.Router();

router.get('/', checkToken, (req, res) => {
  user.getUsers()
    .then(users => {
      res
        .json({ users })
    })
    .catch(error => {
      res
        .status(404)
        .send({ error })
    });
});

router.get('/:id', (req, res) => {
  user.getUserById(req.params.id)
    .then(user => {
      if (user) {
        return res.json({ user });
      }
      res.status(404).json({ errors: ['User not exist'] });
    })
    .catch(error => {
      res
        .status(404)
        .send({ error })
    });
});

router.put('/:id', validateUpdateUser ,(req, res) => {
  user.updateUser(req.params.id, req.body)
    .then(user => {
      if (!user) {
        return res.status(404).json({ error: ['User not exist'] });
      }
      res.json({ status: 'User was updated!' });
    })
    .catch(error => {
      res
        .status(400)
        .json({ error });
    });

});

router.delete('/:id', (req, res) => {
  user.removeUser(req.params.id)
    .then(() => {
      res
        .json({ status: 'The user was removed!' });
    })
    .catch(error => {
      res
        .status(400)
        .json({ error })
    });
});

module.exports = router;