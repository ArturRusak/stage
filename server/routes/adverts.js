'use strict';

const express = require('express');
const controller = require('../controllers/advert');
const {
  validateCreateAdvert,
  validateUpdateAdvert
} = require ('../middleware/validator');

const router = express.Router();

router.get('/', (req, res) => {
  controller.getAdverts()
    .then(adverts => {
      res
        .json({ adverts })
    })
    .catch(error => {
      res
        .status(404)
        .send({ error })
    });
});

router.get('/:id', (req, res) => {
  controller.getAdvertById(req.params.id)
    .then(advert => {
      if (advert) {
        return res.json({ advert });
      }
      res.status(404).json({ errors: ['Advert not exist'] });
    })
    .catch(error => {
      res
        .status(404)
        .send({ error })
    });
});

router.put('/:id', validateUpdateAdvert ,(req, res) => {
  controller.updateAdvert(req.params.id, req.body)
    .then(advert => {
      if (!advert) {
        return res.status(404).json({ errors: ['Advert not exist'] });
      }
      res.json({ status: 'The advert was updated!' });
    })
    .catch(error => {
      res
        .status(400)
        .json({ error });
    });

});


router.put('/:id/counter' ,(req, res) => {
  controller.updateViewsAdvertCounter(req.params.id)
    .then(advert => {
      if (!advert) {
        return res.status(404).json({ errors: ['Advert not exist'] });
      }
      res.json({ status: 'OK' });
    })
    .catch(error => {
      res
        .status(400)
        .json({ error });
    });

});

router.post('/', validateCreateAdvert ,(req, res) => {
  controller.createAdvert(req.body)
    .then(advert => {
      if (advert) {
        res
          .status(201)
          .json({ status: 'Advert was created!' });
      }
    })
    .catch((error) => {
      res
        .status(400)
        .send({ error });
    });
});

router.delete('/:id', (req, res) => {
  controller.removeAdvert(req.params.id)
    .then(() => {
      res
        .json({ status: 'The advert was removed!' });
    })
    .catch(error => {
      res
        .status(400)
        .json({ error })
    });
});

module.exports = router;