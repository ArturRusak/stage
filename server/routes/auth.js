'use strict';

const express = require('express');
const user = require('../controllers/user');
const auth = require('../controllers/auth');

const {
  validateCreateUser,
  validateLogin
} = require ('../middleware/validator');

const router = express.Router();

router.post('/registration', validateCreateUser, (req, res) => {
  user.createUser(req)
    .then(user => {
      if (user) {
        res
          .status(201)
          .json({ status: 'User was registered!' });
      }
    })
    .catch((error) => {
      res
        .status(400)
        .send({ error });
    });
});

router.post('/login', validateLogin, auth.signIn ,() => {
  console.log('SUCCESSFUL LOGIN!!!');
});

router.get('/logout', (req, res) => {});

module.exports = router;