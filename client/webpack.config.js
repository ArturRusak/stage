const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const StyleLinterPlugin = require('stylelint-webpack-plugin');

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: path.join(__dirname, './index.html'),
  filename: 'index.html',
  inject: 'body',
});

const StyleLinterPluginConfig = new StyleLinterPlugin();

module.exports = {
  entry: path.join(__dirname, './src/index.js'),
  output: {
    path: path.resolve('dist'),
    filename: 'index_bundle.js',
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    port: 3000,
    proxy: {
      '/': {
        target: 'http://localhost:3001',
        secure: false,
      },
    },
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|svg|jpg|gif|ico)$/,
        use: [
          'file-loader?name=[name].[ext]',
        ],
      },
      {
        test: /\.(js|jsx)$/,
        exclude: [ /joi-browser/, /node_modules/],
        use: ['babel-loader', 'eslint-loader'],

      },
    ],
  },
  resolve: {
    alias: {
      joi: 'joi-browser'
   }
  },
  plugins: [HtmlWebpackPluginConfig, StyleLinterPluginConfig],
};
