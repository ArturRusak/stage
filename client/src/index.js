import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './components/App';
import configureStore from './store/configureStore';
import '../assets/images/favicon.ico';
import '../assets/css/resest.css';
import '../assets/css/index.css';
import checkResponseToken from './services/checkResponseToken';
import setAuthorizationToken from './services/setAuthorizationToken';
import { login } from './actions/action-creators/auth';

const store = configureStore();

checkResponseToken();
if (localStorage.token) {
  setAuthorizationToken(localStorage.token);
  store.dispatch(login(localStorage.token));
}

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
);
