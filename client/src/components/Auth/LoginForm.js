import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  LOGIN_HEADER,
  VAIDATE_LENGTH
} from '../../constants';
import Button from '../Common/Button';
import validateField from '../../services/validationFields';


class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      password: ''
    };
  }

  handleInputChange(event) {
    const { value, name } = event.target;
    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const { login } = this.props;

    const { userName, password } = this.state;
    const userNameError = validateField(VAIDATE_LENGTH, userName);
    const passwordError = validateField(VAIDATE_LENGTH, password);
    const isError = userNameError || passwordError;

    if (isError) {
      this.setState({
        userNameError,
        passwordError
      });
      return;
    }
    this.setState({
      userNameError: null,
      passwordError: null,
    });

    login({ userName, password });
  }

  render() {
    const {
      userName,
      userNameError,
      password,
      passwordError
    } = this.state;

    return (
      <form
        className="form"
        onSubmit={event => this.handleSubmit(event)}
      >
        <h1>{LOGIN_HEADER}</h1>
        <div className="form__body">
          <div className="form__item">
            <input
              type="text"
              id="user-name"
              name="userName"
              placeholder="User name"
              value={userName}
              onChange={event => this.handleInputChange(event)}
            />
            <label htmlFor="user-name">User Name*</label>
            {userNameError ? <span className="error">{userNameError}</span> : null}
          </div>
          <div className="form__item">
            <input
              type="password"
              id="password"
              name="password"
              placeholder="Enter password"
              value={password}
              onChange={event => this.handleInputChange(event)}
            />
            <label htmlFor="password">Password*</label>
            {passwordError ? <span className="error">{passwordError}</span> : null}
          </div>
          <div className="form__item form-group-actions">
            <Button
              className="form__action"
              onHandleClick={event => this.handleSubmit(event)}
            >
              <span>Login</span>
            </Button>
          </div>
        </div>
      </form>
    );
  }
}

LoginForm.propTypes = {
  login: PropTypes.func.isRequired
};

export default LoginForm;
