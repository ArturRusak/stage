import React from 'react';
import PropTypes from 'prop-types';
import './ServerEror.css';

/**
 * Represents of an error
 * @constructor
 * @returns {*} - Error message
 */
const ServerError = ({ error }) => (
  <div className="server-error">
    <p>{error}</p>
  </div>
);

ServerError.propTypes = {
  error: PropTypes.string.isRequired
};

export default ServerError;
