import React from 'react';
import './NotFound.css';

/**
 * Represents a page not found
 * @constructor
 * @returns {XML} - component not found
 */
const NotFound = () => (
  <div className="container">
    <div className="not-found">
      <div className="not-found-404">
        <h3>Oops! Page not found</h3>
        <h1>404</h1>
      </div>
      <h2>we are sorry, but the page you requested was not found</h2>
    </div>
  </div>
);

export default NotFound;
