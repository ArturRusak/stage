import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import parseDate from '../../services/parseDate';
import Message from '../Common/Message';
import {
  ADVERT_TABLE_COLUMNS_TITLE,
  EMPTY_ADVERTS_LIST
} from '../../constants';

/**
 * Advert item view
 * @function renderAdvertsRows
 * @param {object} adverts - Advert data
 * @param {function} removeAdvert - Remove method of Advert
 * @returns {XML} - Advert row
 */
function renderAdvertsRows(adverts, removeAdvert) {
  return adverts.map((item, count) => (
    <tr key={count} className="table-row-item">
      <td>{item.title}</td>
      <td>{parseDate(item.created)}</td>
      <td>{parseDate(item.modified)}</td>
      <td>{item.description}</td>
      <td>{item.category}</td>
      <td>{item.price}</td>
      <td>{item.views}</td>
      <td>
        <div className="table-row-item-buttons">
          <Link
            className="show-info"
            to={`/adverts/${item.id}`}
          >
            <i className="fa fa-info" />
          </Link>
          <Link
            className="edit-item"
            to={`/edit-advert/${item.id}`}
          >
            <i className="fa fa-indent" aria-hidden="true" />
          </Link>
          <button
            type="button"
            className="remove-item"
            onClick={() => removeAdvert(item.id)}
          >
            <i className="fa fa-trash" />
          </button>
        </div>
      </td>
    </tr>
  ));
}

/**
 * Advert item view
 * @param {object} item - Advert data
 * @param {function} onAdvertRemove - Remove method of Advert
 * @constructor AdvertsTable
 * @returns {XML} - Adverts table
 */
const AdvertsTable = ({ adverts, onAdvertRemove }) => {
  if (adverts.length) {
    return (
      <div className="table-wrapper">
        <table className="store-table store-table_main advert">
          <thead className="store-table__header table-col">
            <tr>
              {
                ADVERT_TABLE_COLUMNS_TITLE.map((element, index) => (
                  <th key={index}>
                    {element.label}
                  </th>
                ))
              }
            </tr>
          </thead>
          <tbody>
            {renderAdvertsRows(adverts, onAdvertRemove)}
          </tbody>
        </table>
      </div>
    );
  }
  return (
    <div className="additional-info">
      <Message
        className="no-data"
        content={EMPTY_ADVERTS_LIST}
      />
    </div>
  );
};

AdvertsTable.propTypes = {
  adverts: PropTypes.arrayOf(PropTypes.object).isRequired,
  onAdvertRemove: PropTypes.func.isRequired
};

export default AdvertsTable;
