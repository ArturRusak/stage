import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import validateField from '../../services/validationFields';
import isEquivalent from '../../services/compareObjects';
import Button from '../Common/Button';
import Message from '../Common/Message';
import {
  EDIT_ADVERT,
  ADD_ADVERT,
  TITLE,
  DESCRIPTION,
  CATEGORY,
  PRICE
} from '../../constants';

/**
 * Users form
 */
class AdvertForm extends Component {
  /**
   * Getting the props
   * @param {Object} props - Methods and data
   */
  constructor(props) {
    super(props);
    const { advert } = props;
    this.defaultAdvertData = {
      title: '',
      description: '',
      category: '',
      price: '',
      user_id: ''
    };
    this.state = {
      advertData: advert ? { ...advert } : this.defaultAdvertData,
      pushed: false
    };
  }

  /**
   * componentDidUpdate
   * @param {Object} prevProps - prev props
   * @returns {*} -
   */
  componentDidUpdate(prevProps) {
    const { createdAdvert, modifiedAdvert } = this.props;
    const isSuccessModify = (prevProps.modifiedAdvert.status
      !== modifiedAdvert.status) && !modifiedAdvert.error;
    const isSuccessCreated = (prevProps.createdAdvert.status
      !== createdAdvert.status) && !createdAdvert.error;

    if (isSuccessCreated || isSuccessModify) {
      this.setState({
        advertData: { ...this.defaultAdvertData }
      });
    }
  }

  /**
   * Confirm information
   * @param {Object} event - Event of submit
   * @returns {void}
   */
  handleSubmit(event) {
    event.preventDefault();

    const { onCreateAdvert, onModifyAdvert, advert } = this.props;
    const { advertData, pushed } = this.state;
    const titleError = validateField(TITLE, advertData.title);
    const descriptionError = validateField(DESCRIPTION, advertData.description);
    const categoryError = validateField(CATEGORY, advertData.category);
    const priceError = validateField(PRICE, advertData.price);

    if (titleError || descriptionError || categoryError || priceError) {
      this.setState({
        titleError,
        descriptionError,
        categoryError,
        priceError,
        pushed: false
      });
      return;
    }
    this.setState({
      titleError: null,
      descriptionError: null,
      categoryError: null,
      priceError: null,
      pushed: !pushed
    });

    if (advert) {
      const {
        id, title, description, category, price
      } = advertData;
      const finalAdvert = {
        id,
        title,
        description,
        category,
        price
      };
      const isEquivalentObjects = isEquivalent(advert, advertData);

      if (!isEquivalentObjects) {
        onModifyAdvert(finalAdvert);
      }
      return;
    }
    onCreateAdvert(advertData);
  }

  /**
   * Change the value of input field
   * @param {Object} event - Event object
   * @returns {void}
   */
  handleChangeField(event) {
    const { advertData } = this.state;
    const { name, value } = event.target;
    this.setState({
      advertData: {
        ...advertData,
        [name]: value
      }
    });
  }

  /**
   * Change the value of select
   * @param {Object} event - Event object
   * @returns {void}
   */
  handleSelectChange(event) {
    const { value } = event.target;
    const { advertData } = this.state;
    const { users } = this.props;
    let id = '';
    if (value !== 'Choose user') {
      [{ id }] = users.filter(item => item.id === parseInt(value, 10));
    }

    this.setState({
      advertData: {
        ...advertData,
        user_id: id
      }
    });
  }

  /**
   * Change the option value of select
   * @returns {*} - select
   */
  bindUsers() {
    const { users } = this.props;
    return (
      <select onChange={event => this.handleSelectChange(event)}>
        <option defaultValue="">Choose user</option>
        {
          users.map(
            user => (
              <option key={user.id} value={user.id}>
                {`${user.userName}`}
              </option>
            ),
          )
        }
      </select>
    );
  }

  /**
   * Show users List
   * @returns {*} - users list
   */
  showUsersList() {
    const { users } = this.props; // TODO CHECK , must be refactoring

    if (users.length) {
      return (
        <div className="form__item">{this.bindUsers()}</div>
      );
    }
    return null;
  }

  /**
   * Represents form
   * @returns {XML} - Users form
   */
  render() {
    const {
      advertData,
      titleError,
      descriptionError,
      categoryError,
      priceError,
      pushed
    } = this.state;
    const {
      modifiedAdvert,
      createdAdvert,
      isEditForm
    } = this.props;
    const successRequestMessage = modifiedAdvert.status || createdAdvert.status;

    return (
      <form
        className="form"
        onSubmit={event => this.handleSubmit(event)}
      >
        <div className="form__body">
          <div className="form__item">
            <input
              type="text"
              name="title"
              id="title"
              placeholder="Title"
              value={advertData.title}
              onChange={event => this.handleChangeField(event)}
            />
            <label htmlFor="title">Title*</label>
            {titleError ? <span className="error">{titleError}</span> : null}
          </div>
          <div className="form__item">
            <input
              type="text"
              name="description"
              id="description"
              placeholder="Description"
              value={advertData.description}
              onChange={event => this.handleChangeField(event)}
            />
            <label htmlFor="description">Description*</label>
            {descriptionError ? <span className="error">{descriptionError}</span> : null}
          </div>
          <div className="form__item">
            <input
              type="text"
              name="category"
              id="category"
              placeholder="Category"
              value={advertData.category}
              onChange={event => this.handleChangeField(event)}
            />
            <label htmlFor="category">Category*</label>
            {categoryError ? <span className="error">{categoryError}</span> : null}
          </div>
          <div className="form__item">
            <input
              type="price"
              name="price"
              id="price"
              placeholder="Price"
              value={advertData.price}
              onChange={event => this.handleChangeField(event)}
            />
            <label htmlFor="price">Price*</label>
            {priceError ? <span className="error">{priceError}</span> : null}
          </div>
          {this.showUsersList()}
          {successRequestMessage && pushed && (
            <Message
              className="form__item success-message"
              content={successRequestMessage}
            />
          )}
          <div className="form__item form-group-actions">
            <Button
              className="form__action"
              onHandleClick={event => this.handleSubmit(event)}
            >
              <span>{isEditForm ? EDIT_ADVERT : ADD_ADVERT}</span>
            </Button>
            <Link
              className="form__action"
              to="/adverts"
            >
              Back
            </Link>
          </div>
        </div>
      </form>
    );
  }
}

AdvertForm.defaultProps = {
  advert: null
};

AdvertForm.propTypes = {
  advert: PropTypes.arrayOf(PropTypes.object),
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
  createdAdvert: PropTypes.shape({
    status: PropTypes.string,
    error: PropTypes.string,
    loading: PropTypes.bool
  }).isRequired,
  modifiedAdvert: PropTypes.shape({
    status: PropTypes.string,
    error: PropTypes.string,
    loading: PropTypes.bool
  }).isRequired,
  isEditForm: PropTypes.bool.isRequired,
  onCreateAdvert: PropTypes.func.isRequired,
  onModifyAdvert: PropTypes.func.isRequired
};

export default AdvertForm;
