import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import parseDate from '../../services/parseDate';
import { ADVERT_TABLE_COLUMNS_TITLE, EMPTY_ADVERTS_LIST } from '../../constants';
import Message from '../Common/Message';

/**
 * Represent of Advert item
 * @param {Array} adverts - adverts data
 * @param {function} removeAdvert - Remove method of Advert
 * @returns {*} - advert
 */
function renderAdvert(adverts, removeAdvert) {
  return adverts.map((item, count) => (
    <tr key={count} className="table-row-item">
      <td>{item.title}</td>
      <td>{parseDate(item.created)}</td>
      <td>{parseDate(item.modified)}</td>
      <td>{item.description}</td>
      <td>{item.category}</td>
      <td>{item.price}</td>
      <td>{item.views}</td>
      <td>
        <div className="table-row-item-buttons">
          <Link
            className="edit-item"
            to={`/edit-advert/${item.id}`}
          >
            <i className="fa fa-indent" aria-hidden="true" />
          </Link>
          <button
            type="button"
            className="remove-item"
            onClick={() => removeAdvert(item.id)}
          >
            <i className="fa fa-trash" />
          </button>
        </div>
      </td>
    </tr>
  ));
}

/**
 * Advert item view
 * @param {Array} advertData - all adverts and user
 * @param {function} onAdvertRemove - Remove method of Advert
 * @returns {XML} - Adverts table
 */
const AdvertInfo = ({ advertData, onAdvertRemove }) => {
  if (advertData.length) {
    const validAdverts = advertData.filter(item => item.user_id);
    if (validAdverts.length) {
      return (
        <div className="table-wrapper">
          <table className="store-table store-table_main advert">
            <thead className="store-table__header table-col">
              <tr>
                {
                  ADVERT_TABLE_COLUMNS_TITLE.map((element, index) => (
                    <th key={index}>
                      {element.label}
                    </th>
                  ))
                }
              </tr>
            </thead>
            <tbody>
              {renderAdvert(validAdverts, onAdvertRemove)}
            </tbody>
          </table>
        </div>
      );
    }
  }
  return (
    <Message className="no-data" content={EMPTY_ADVERTS_LIST} />
  );
};

AdvertInfo.defaultProps = {
  advertData: []
};

AdvertInfo.propTypes = {
  advertData: PropTypes.arrayOf(PropTypes.object),
  onAdvertRemove: PropTypes.func.isRequired,
};

export default AdvertInfo;
