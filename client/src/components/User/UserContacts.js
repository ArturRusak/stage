import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  USER_DETAILS,
  USER_NAME_TITLE,
  PHONE_TITLE,
  EMAIL_TITLE,
  SHOW_ALL_USER_ADVERTS
} from '../../constants';

const UserContacts = ({ userData }) => {
  if (userData.length) {
    const {
      firstName, phone, email, user_id
    } = userData[0];
    return (
      <div className="additional-info">
        <h2>{USER_DETAILS}</h2>
        <ul className="details-board">
          <li className="details-board__list-item">
            <span className="details-board__title">
              {USER_NAME_TITLE}:
            </span>
            {firstName}
          </li>
          <li className="details-board__list-item">
            <span className="details-board__title">
              {PHONE_TITLE}:
            </span>
            <a href={`tel:${phone}`}>{phone}</a>
          </li>
          <li className="details-board__list-item">
            <span className="details-board__title">
              {EMAIL_TITLE}:
            </span>
            <a href={`mailto:${email}`}>{email}</a>
          </li>
          <li className="details-board__list-item text-center">
            <Link
              className="action-btn"
              to={`/users/${user_id}`}
            >
              {SHOW_ALL_USER_ADVERTS}
            </Link>
          </li>
        </ul>
      </div>
    );
  }
  return null;
};

UserContacts.propTypes = {
  userData: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default UserContacts;
