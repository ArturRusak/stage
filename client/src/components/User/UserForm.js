import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import validateField from '../../services/validationFields';
import isEquivalent from '../../services/compareObjects';
import Button from '../Common/Button';
import Message from '../Common/Message';
import {
  LOGIN_BUTTON,
  EDIT_USER,
  REGISTRATION,
  USER_NAME,
  LAST_NAME,
  EMAIL,
  PHONE,
  FIRST_NAME,
  PASSWORD
} from '../../constants';

/**
 * Users form
 */
class UserForm extends Component {
  /**
   * Getting the props
   * @param {Object} props - Methods and data
   */
  constructor(props) {
    super(props);
    const { user } = props;
    this.defaultUserData = {
      userName: '',
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      password: ''
    };
    this.state = {
      userData: user ? { ...user } : this.defaultUserData,
      pushed: false
    };
  }

  /**
   * componentDidUpdate
   * @param {Object} prevProps - prev props
   * @returns {*} -
   */
  componentDidUpdate(prevProps) {
    const { createdUser, modifiedUser } = this.props;
    const isSuccessModify = (prevProps.modifiedUser.status
      !== modifiedUser.status) && !modifiedUser.error;
    const isSuccessCreated = (prevProps.createdUser.status
      !== createdUser.status) && !createdUser.error;

    if (isSuccessCreated || isSuccessModify) {
      this.setState({
        userData: { ...this.defaultUserData }
      });
    }
  }

  /**
   * Confirm information
   * @param {Object} event - Event of submit
   * @returns {void}
   */
  handleSubmit(event) {
    event.preventDefault();

    const { onCreateUser, onModifyUser, user } = this.props;
    const { userData, pushed } = this.state;
    const emailError = validateField(EMAIL, userData.email);
    const userNameError = validateField(USER_NAME, userData.userName);
    const firstNameError = validateField(FIRST_NAME, userData.firstName);
    const lastNameError = validateField(LAST_NAME, userData.lastName);
    const phoneError = validateField(PHONE, userData.phone);
    const passwordError = validateField(PASSWORD, userData.password);
    const isError = firstNameError || userNameError || lastNameError
      || emailError || phoneError || passwordError;

    if (isError) {
      this.setState({
        emailError,
        userNameError,
        firstNameError,
        lastNameError,
        phoneError,
        passwordError,
        pushed: false
      });
      return;
    }
    this.setState({
      emailError: null,
      userNameError: null,
      firstNameError: null,
      lastNameError: null,
      phoneError: null,
      passwordError: null,
      pushed: !pushed
    });

    if (user) {
      const isEquivalentObjects = isEquivalent(user, userData);
      if (!isEquivalentObjects) {
        onModifyUser(userData);
      }
      return;
    }
    onCreateUser(userData);
  }

  /**
   * Change the value of input field
   * @param {Object} event - Event object
   * @returns {void}
   */
  handleChangeField(event) {
    const { userData } = this.state;
    const { name, value } = event.target;
    this.setState({
      userData: {
        ...userData,
        [name]: value
      }
    });
  }

  /**
   * Represents form
   * @returns {*} - Users form
   */
  render() {
    const { modifiedUser, createdUser, isEditForm } = this.props;
    const successRequestMessage = modifiedUser.status || createdUser.status;
    const {
      userData,
      userNameError,
      firstNameError,
      lastNameError,
      emailError,
      phoneError,
      passwordError,
      pushed
    } = this.state;
    return (
      <form
        className="form"
        onSubmit={event => this.handleSubmit(event)}
      >
        <div className="form__body">
          <div className="form__item">
            <input
              type="text"
              name="userName"
              id="user-name"
              placeholder="User Name"
              value={userData.userName}
              onChange={event => this.handleChangeField(event)}
            />
            <label htmlFor="user-name">User Name*</label>
            {userNameError ? <span className="error">{userNameError}</span> : null}
          </div>
          <div className="form__item">
            <input
              type="text"
              name="firstName"
              id="first-name"
              placeholder="First Name"
              value={userData.firstName}
              onChange={event => this.handleChangeField(event)}
            />
            <label htmlFor="first-name">First Name*</label>
            {firstNameError ? <span className="error">{firstNameError}</span> : null}
          </div>
          <div className="form__item">
            <input
              type="text"
              name="lastName"
              id="last-name"
              placeholder="Last Name"
              value={userData.lastName}
              onChange={event => this.handleChangeField(event)}
            />
            <label htmlFor="last-name">Last Name*</label>
            {lastNameError ? <span className="error">{lastNameError}</span> : null}
          </div>
          <div className="form__item">
            <input
              type="email"
              name="email"
              id="email"
              placeholder="email"
              value={userData.email}
              onChange={event => this.handleChangeField(event)}
            />
            <label htmlFor="email">Email*</label>
            {emailError ? <span className="error">{emailError}</span> : null}
          </div>
          <div className="form__item">
            <input
              type="phone"
              name="phone"
              id="phone"
              placeholder="phone"
              value={userData.phone}
              onChange={event => this.handleChangeField(event)}
            />
            <label htmlFor="phone">phone*</label>
            {phoneError ? <span className="error">{phoneError}</span> : null}
          </div>
          <div className="form__item">
            <input
              type="password"
              name="password"
              id="password"
              placeholder="password"
              value={userData.password}
              onChange={event => this.handleChangeField(event)}
            />
            <label htmlFor="password">password*</label>
            {passwordError ? <span className="error">{passwordError}</span> : null}
          </div>
          {successRequestMessage && pushed ? (
            <Message
              className="form__item success-message"
              content={successRequestMessage}
            />
          ) : null}
          <div className="form__item form-group-actions">
            <Button
              className="form__action"
              onHandleClick={event => this.handleSubmit(event)}
            >
              <span>{isEditForm ? EDIT_USER : REGISTRATION}</span>
            </Button>
            <Link
              className="form__action"
              to="/login"
            >
              {LOGIN_BUTTON}
            </Link>
          </div>
        </div>
      </form>
    );
  }
}

UserForm.defaultProps = {
  user: null
};

UserForm.propTypes = {
  createdUser: PropTypes.shape({
    status: PropTypes.string,
    error: PropTypes.string,
    loading: PropTypes.bool
  }).isRequired,
  modifiedUser: PropTypes.shape({
    status: PropTypes.object,
    error: PropTypes.string,
    loading: PropTypes.bool
  }).isRequired,
  user: PropTypes.shape(),
  isEditForm: PropTypes.bool.isRequired,
  onCreateUser: PropTypes.func.isRequired,
  onModifyUser: PropTypes.func.isRequired
};

export default UserForm;
