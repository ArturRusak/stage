import React from 'react';
import PropTypes from 'prop-types';
import Message from '../Common/Message';
import {
  EMPTY_ADVERTS_LIST,
  USER_DETAILS_TABLE_COLUMNS_TITLE
} from '../../constants';

/**
 * Represent of user item
 * @param {Object} item - user data
 * @returns {*} - user
 */
function renderUser(item) {
  return (
    <tr className="table-row-item">
      <td>{item.userName}</td>
      <td>{item.firstName}</td>
      <td>{item.lastName}</td>
      <td>{item.email}</td>
      <td>{item.phone}</td>
    </tr>
  );
}

/**
 * User item view
 * @param {Array} user - all adverts and User
 * @constructor UsersTable
 * @returns {XML} - Users table
 */
const UserInfo = ({ user }) => {
  if (user.length) {
    return (
      <div className="table-wrapper">
        <table className="store-table users">
          <thead>
            <tr className="store-table__header">
              {
                USER_DETAILS_TABLE_COLUMNS_TITLE.map((element, index) => (
                  <th key={index}>
                    {element.label}
                  </th>
                ))
              }
            </tr>
          </thead>
          <tbody>
            {renderUser(user[0])}
          </tbody>
        </table>
      </div>
    );
  }
  return <Message className="no-data" content={EMPTY_ADVERTS_LIST} />;
};

UserInfo.propTypes = {
  user: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default UserInfo;
