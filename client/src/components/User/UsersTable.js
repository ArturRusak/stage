import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { USER_TABLE_COLUMNS_TITLE, EMPTY_USER_LIST } from '../../constants';
import Message from '../Common/Message';

/**
 * User item view
 * @function renderUsersRows
 * @param {object} users - User data
 * @param {function} onUserRemove - Remove method of User
 * @returns {XML} - User row
 */
function renderUsersRows(users, onUserRemove) {
  return users.map(item => (
    <tr className="table-row-item" key={item.id}>
      <td>{item.userName}</td>
      <td>{item.firstName}</td>
      <td>{item.lastName}</td>
      <td>{item.email}</td>
      <td>{item.phone}</td>
      <td>
        <div className="table-row-item-buttons">
          <Link
            className="show-info"
            to={`/users/${item.id}`}
          >
            <i className="fa fa-info" />
          </Link>
          <Link
            className="edit-item"
            to={`/edit-user/${item.id}`}
          >
            <i className="fa fa-indent" aria-hidden="true" />
          </Link>
          <button
            type="button"
            className="remove-item"
            onClick={() => onUserRemove(item.id)}
          >
            <i className="fa fa-trash" />
          </button>
        </div>
      </td>
    </tr>
  ));
}

/**
 * User item view
 * @param {object} item - User data
 * @param {function} onUserRemove - Remove method of User
 * @constructor UsersTable
 * @returns {XML} - Users table
 */
const UsersTable = ({ users, onUserRemove }) => {
  if (users.length) {
    return (
      <div className="table-wrapper">
        <table className="store-table store-table_main users">
          <thead>
            <tr className="store-table__header">
              {
                USER_TABLE_COLUMNS_TITLE.map((element, index) => (
                  <th key={index}>
                    {element.label}
                  </th>
                ))
              }
            </tr>
          </thead>
          <tbody>
            {renderUsersRows(users, onUserRemove)}
          </tbody>
        </table>
      </div>
    );
  }
  return <Message className="no-data" content={EMPTY_USER_LIST} />;
};

UsersTable.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
  onUserRemove: PropTypes.func.isRequired
};

export default UsersTable;
