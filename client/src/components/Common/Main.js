import React from 'react';
import Routes from '../../routes';

/**
 * Container of content app
 * @constructor
 * @returns {String} - Stateless component
 */
const Main = () => (
  <section className="content">
    <Routes />
  </section>
);

export default Main;
