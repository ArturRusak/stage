import React from 'react';
import PropTypes from 'prop-types';

/**
 * Common button
 * @param {Object} props - Object of properties
 * @constructor Button
 * @returns {*} - Button element
 */
const Button = (props) => {
  const { children, onHandleClick, className } = props;
  return (
    <button
      type="button"
      className={className}
      onClick={onHandleClick}
    >
      {children}
    </button>
  );
};

Button.defaultProps = {
  className: ''
};

Button.propTypes = {
  onHandleClick: PropTypes.func.isRequired,
  className: PropTypes.string,
  children: PropTypes.element.isRequired
};

export default Button;
