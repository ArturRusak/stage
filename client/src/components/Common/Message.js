import React from 'react';
import PropTypes from 'prop-types';

/**
 * Represents a message
 * @constructor
 * @returns {*} - Message no data
 */
const Message = ({ className, content }) => (
  <div className={className}>
    <p>{content}</p>
  </div>
);

Message.defaultProps = {
  className: '',
};

Message.propTypes = {
  className: PropTypes.string,
  content: PropTypes.string.isRequired
};

export default Message;
