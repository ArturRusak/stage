import React from 'react';
import './Spinner.css';

/**
 * Represents a spinner
 * @constructor
 * @returns {XML} - loading indicator
 */
const LOADING_DATA = 'Loading data...';
const Spinner = () => (
  <div className="loading">
    <div className="loading__content">
      <div className="lds-ripple">
        <div />
        <div />
      </div>
      <span className="loading__message">
        {LOADING_DATA}
      </span>
    </div>
  </div>
);

export default Spinner;
