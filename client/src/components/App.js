import React from 'react';
import Header from './Header/Header';
import Footer from './Footer/Footer';
import Main from './Common/Main';

/**
 * Main Component containing all components
 * @constructor App
 * @returns {String} - Page
 */
const App = () => (
  <div className="flex-container">
    <Header />
    <Main />
    <Footer />
  </div>
);

export default App;
