import React from 'react';
import Navigation from '../../containers/Navigation/Navigation';
import './Header.css';

/**
 * Represents a header
 * @constructor
 * @returns {String} - Stateless component
 */
const Header = () => (
  <header className="header">
    <div className="container header__container">
      <div className="logo togo_top">
        <i className="fa fa-book fa-2x" />
        <span className="logo__text">Adverts board</span>
      </div>
      <Navigation />
    </div>
  </header>
);
export default Header;
