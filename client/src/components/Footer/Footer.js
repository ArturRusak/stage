import React from 'react';
import './Footer.css';

/**
 * Represents a footer
 * @constructor
 * @returns {XML} - footer
 */
const Footer = () => (
  <footer className="footer">
    <div className="container footer__container">
      <div className="logo togo_small">
        <i className="fa fa-advert fa-2x" />
        <span className="logo__text">Adverts board</span>
      </div>
    </div>
  </footer>
);

export default Footer;
