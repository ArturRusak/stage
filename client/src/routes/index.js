import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Adverts from '../containers/Adverts/Adverts';
import AdvertDetails from '../containers/Adverts/AdvertsDetails';
import CreateAndEditUser from '../containers/Users/CreateAndEditUser';
import CreateAndEditAdvert from '../containers/Adverts/CreateAndEditAdvert';
import Login from '../containers/Auth/Login';
import UserList from '../containers/Users/Users';
import UserDetails from '../containers/Users/UserDetails';
import NotFound from '../components/NotFound/NotFound';

import requireAuth from '../services/requireAuth';

/**
 * Setting up of app routes
 * @returns {String} - Stateless component
 */
const Routes = () => (
  <Switch>
    <Route exact path="/" component={Adverts} />
    <Route exact path="/login" component={Login} />
    <Route exact path="/adverts" component={Adverts} />
    <Route exact path="/adverts/:id" component={requireAuth(AdvertDetails)} />
    <Route exact path="/new-advert/" component={requireAuth(CreateAndEditAdvert)} />
    <Route exact path="/edit-advert/:id" component={requireAuth(CreateAndEditAdvert)} />
    <Route exact path="/users" component={requireAuth(UserList)} />
    <Route exact path="/users/:id" component={requireAuth(UserDetails)} />
    <Route exact path="/registration/" component={CreateAndEditUser} />
    <Route exact path="/edit-user/:id" component={requireAuth(CreateAndEditUser)} />
    <Route component={NotFound} />
  </Switch>
);

export default Routes;
