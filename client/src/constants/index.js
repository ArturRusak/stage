export const USERS = 'Users';
export const ADVERTS = 'Adverts';
export const LOGIN = 'Login';
export const URL = '/api';
export const BACK = 'Back';

// Headers, titles
export const CREATE_NEW_USER = 'Create a new user';
export const EDIT_TARGET_USER = 'Edit the user';
export const EMPTY_USER_LIST = 'User list is empty';
export const ALL_USERS = 'All users';
export const USER_DETAILS = 'About user';
export const USER_ADVERTS = 'User\'s adverts';
export const USER_NAME_TITLE = 'User name';
export const PHONE_TITLE = 'Phone';
export const EMAIL_TITLE = 'Email';


// Buttons
export const EDIT_USER = 'Edit user';
export const SHOW_ALL_USER_ADVERTS = 'Show all user\'s adverts';

// Name fields form for user
export const USER_NAME = 'USER_NAME';
export const FIRST_NAME = 'FIRST_NAME';
export const LAST_NAME = 'LAST_NAME';
export const PHONE = 'PHONE';
export const EMAIL = 'EMAIL';
export const PASSWORD = 'PASSWORD';

export const VAIDATE_LENGTH = 'VALIDATE_LENGTH';

// AUTH
export const LOGIN_HEADER = 'Login';
export const LOGIN_BUTTON = 'Go to login';
export const REGISTRATION = 'Registration';

// Titles of table columns
export const USER_TABLE_COLUMNS_TITLE = [
  {
    label: 'User Name'
  },
  {
    label: 'First Name'
  },
  {
    label: 'Last Name'
  },
  {
    label: 'E-mail'
  },
  {
    label: 'Phone'
  },
  {
    label: 'Actions'
  }
];
export const USER_DETAILS_TABLE_COLUMNS_TITLE = [
  {
    label: 'User Name'
  },
  {
    label: 'First Name'
  },
  {
    label: 'Last Name'
  },
  {
    label: 'E-mail'
  },
  {
    label: 'Phone'
  }
];

// ------------ADVERTS

// Headers, titles
export const EMPTY_ADVERTS_LIST = 'Adverts list is empty';
export const ADVERT_DETAILS = 'Advert details';
export const ALL_ADVERTS = 'All adverts';
export const ADD_NEW_ADVERT = 'Add a new advert';
export const EDIT_ADVERT_TITLE = 'Edit the advert';

// Buttons
export const ADD_ADVERT = 'Add advert';
export const EDIT_ADVERT = 'Edit advert';

// Name fields form for advert
export const TITLE = 'TITLE';
export const DESCRIPTION = 'DESCRIPTION';
export const CATEGORY = 'CATEGORY';
export const PRICE = 'PRICE';

// Titles of table columns
export const ADVERT_TABLE_COLUMNS_TITLE = [
  {
    label: 'Title'
  },
  {
    label: 'Created'
  },
  {
    label: 'Modified'
  },
  {
    label: 'Description'
  },
  {
    label: 'Category'
  },
  {
    label: 'Price'
  },
  {
    label: 'Views'
  },
  {
    label: 'Actions'
  }
];
