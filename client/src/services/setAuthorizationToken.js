import Axios from 'axios';

export default function setAuthorizationToken(accessToken) {
  /**
   * Adding authorization header to all requests
   * @param {Object} config - Config for request
   * @returns {void}
   */
  if (accessToken) {
    Axios.interceptors.request.use((data) => {
      const config = { ...data };

      if (accessToken) {
        config.headers.Authorization = `Bearer ${accessToken}`;
      }

      return config;
    });
  }
}
