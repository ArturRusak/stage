import React from 'react';
import Axios from 'axios';
import { Redirect } from 'react-router-dom';
import { logOutAction } from '../actions/action-creators/auth';

/**
 * Checking of success token and redirect to login page
 * @returns {void}
 */
export default function checkResponseToken() {
  Axios.interceptors.response.use(null, (error) => {
    const { data } = error.response;
    if (error.response && !data.success) {
      localStorage.removeItem('token');
      logOutAction();
      return (<Redirect to="/login" />);
    }
    return null;
  });
}
