/**
 * Parse Date from string
 * @param {String} dateString - String from database
 * @returns {*} - Date or '-'
 */
export default function parseDate(dateString) {
  const objDate = new Date(dateString);
  if (objDate.toString() !== 'Invalid Date') {
    let hours = objDate.getHours();
    let dateDay = objDate.getDate();
    let minutes = objDate.getMinutes();
    let month = objDate.getMonth() + 1;
    month = month < 10 ? `0${month}` : month;
    dateDay = dateDay < 10 ? `0${dateDay}` : dateDay;
    hours = hours < 10 ? `0${hours}` : hours;
    minutes = minutes < 10 ? `0${minutes}` : minutes;

    const createdTime = `${hours}:${minutes}`;
    const createdDate = `${dateDay}/${month}/${objDate.getFullYear()}`;
    return `${createdTime} ${createdDate}`;
  }
  return '-';
}
