import Axios from 'axios';
import { URL } from '../../constants';


/**
 * Getting all items
 * @returns {Promise} Promise return list of adverts
 */
export function getAdvertsApi() {
  const endpoint = `${URL}/adverts`;
  return Axios.get(endpoint);
}

/**
 * Getting all items
 * @param {String} id - Unique of item
 * @returns {Promise} Promise return data of advert
 */
export function getAdvertByIdApi(id) {
  const endpoint = `${URL}/adverts/${id}`;
  return Axios.get(endpoint);
}


/**
 * Removing advert
 * @param {String} id - Unique of item
 * @returns {Promise} Promise return list of adverts
 */
export function removeAdvertApi(id) {
  const endpoint = `${URL}/adverts/${id}`;
  return Axios.delete(endpoint);
}

/**
 * Adding data advert
 * @param {Object} item - Data of the item
 * @returns {Promise} Promise return item
 */
export function createAdvertApi(item) {
  const endpoint = `${URL}/adverts/`;
  return Axios.post(endpoint, item);
}

/**
 * Edit data item advert
 * @param {Object} item - Data of the item
 * @returns {Promise} Promise return item
 */
export function modifyAdvertApi(item) {
  const endpoint = `${URL}/adverts/${item.id}`;
  return Axios.put(endpoint, item);
}

/**
 * Update advert view counter
 * @param {String} id - id of item
 * @returns {Promise} Promise return item
 */
export function updateAdvertViewCounterApi(id) {
  const endpoint = `${URL}/adverts/${id}/counter`;
  return Axios.put(endpoint);
}
