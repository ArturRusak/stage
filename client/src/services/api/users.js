import Axios from 'axios';
import { URL } from '../../constants';

/**
 * Getting all items
 * @returns {Promise} Promise return list of users
 */
export function getUsersApi() {
  const endpoint = `${URL}/users`;
  return Axios.get(endpoint);
}

/**
 * Getting all items
 * @param {String} id - Unique of item
 * @returns {Promise} Promise return list of users
 */
export function getUserByIdApi(id) {
  const endpoint = `${URL}/users/${id}`;
  return Axios.get(endpoint);
}

/**
 * Removing user
 * @param {String} id - Unique of item
 * @returns {Promise} Promise return list of users
 */
export function removeUserApi(id) {
  const endpoint = `${URL}/users/${id}`;
  return Axios.delete(endpoint);
}

/**
 * Adding data user
 * @param {Object} item - Data of the item
 * @returns {Promise} Promise return item
 */
export function createUserApi(item) {
  const endpoint = '/registration';
  return Axios.post(endpoint, item);
}

/**
 * Edit data item user
 * @param {Object} item - Data of the item
 * @returns {Promise} Promise return item
 */
export function modifyUserApi(item) {
  const endpoint = `${URL}/users/${item.id}`;
  return Axios.put(endpoint, item);
}
