import Axios from 'axios';

/**
 * Login user
 * @param {Object} credentials - userName, password
 * @returns {Promise} Promise return list of users
 */
export default function loginUserApi(credentials) {
  const endpoint = '/login';
  return Axios.post(endpoint, credentials);
}
