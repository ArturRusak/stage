import {
  USER_NAME,
  FIRST_NAME,
  LAST_NAME,
  TITLE,
  CATEGORY,
  DESCRIPTION,
  EMAIL,
  PRICE,
  PHONE,
  PASSWORD,
  VAIDATE_LENGTH
} from '../constants';

/**
 * Validate string for length
 * @param {String} strValue - String value of input field
 * @param {Number} minLength - min size of length string
 * @param {Number} maxLength - max size of length string
 * @returns {*} - Have error or no error
 */
function validateLength(strValue, minLength = 2, maxLength = 999) {
  if (strValue.length < minLength) {
    return `The input needs to be min ${minLength} characters long`;
  }
  if (strValue.length > maxLength) {
    return `The input needs to be max ${maxLength} characters long`;
  }
  return null;
}

/**
 * Change to number
 * @param {String} value - value of field
 * @returns {*} - number or null
 */
function filterFloat(value) {
  if (/^(-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/.test(value)) {
    return Number(value);
  }
  return null;
}


/**
 * Validate numeric
 * @param {String} value - String value of input field
 * @returns {*} - Have error or no error
 */
function validateNumber(value) {
  let isError = false;
  let error = '';
  const isNamberSings = /^\d*$/; // numbers
  const isValidLength = validateLength(value, 1, 10);
  if (isValidLength) {
    isError = true;
    error = isValidLength;
  }
  const resultValue = filterFloat(value);
  if (!isNamberSings.test(resultValue) && resultValue < 0) {
    isError = true;
    error = 'The price must be a number and positive';
  }

  if (isError) {
    return error;
  }
  return null;
}

/**
 * Validate Email
 * @param {String} email - Emailstring value
 * @returns {*} - error or null
 */
function validateEmail(email) {
  let isError = false;
  let error = '';
  const mailFormat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;

  if (!mailFormat.test(email)) {
    isError = true;
    error = 'Requires valid email';
  }
  if (isError) {
    return error;
  }
  return null;
}

/**
 * Validate of password
 * @param {String} value - password value
 * @returns {*} - Have error or no error
 */
function validatePassword(value) {
  let isError = false;
  let error = '';
  const isValidLength = validateLength(value, 3, 99);
  if (isValidLength) {
    isError = true;
    error = isValidLength;
  }

  if (isError) {
    return error;
  }
  return null;
}

/**
 * Validate string on letters spaces
 * @param {String} fieldName - String value of input field
 * @param {*} strValue - value of field
 * @returns {*} - Have error or no error
 */
function validateString(fieldName, strValue) {
  let isError = false;
  let error = '';
  let isStringLetters = /./; // all signs
  let isValidLength = null;
  // TODO solve problem with regexp eslint
  const latinLettersNumeric = new RegExp(/^[A-Za-z0-9-.]+$/); // USER NAME, TITLE, DESCRIPTION, CATEGORY
  const latinCyrillicSings = new RegExp(/^[A-Za-zА-Яа-я -]+$/); // FIRST NAME
  const latinCyrillic = new RegExp(/^[A-Za-zА-Яа-я]+$/); // LAST NAME
  const phoneNumber = /^\+375\d{9}$/; // PHONE

  if (fieldName === USER_NAME) {
    isStringLetters = latinLettersNumeric;
    isValidLength = validateLength(strValue, 4, 10);
  }
  if (fieldName === TITLE) {
    isStringLetters = latinLettersNumeric;
    isValidLength = validateLength(strValue, 10, 50);
  }
  if (fieldName === DESCRIPTION || fieldName === CATEGORY) {
    isStringLetters = latinLettersNumeric;
  }
  if (fieldName === FIRST_NAME) {
    isStringLetters = latinCyrillicSings;
    isValidLength = validateLength(strValue, 2, 20);
  }
  if (fieldName === LAST_NAME) {
    isStringLetters = latinCyrillic;
    isValidLength = validateLength(strValue, 2, 20);
  }
  if (fieldName === PHONE) {
    isStringLetters = phoneNumber;
  }

  if (isValidLength) {
    isError = true;
    error = isValidLength;
  }
  if (!isStringLetters.test(strValue)) {
    isError = true;
    error = 'The input needs contain only letters';
  }

  if (isError) {
    return error;
  }
  return null;
}

/**
 * Validation of fields
 * @param {String} nameFiled - Name of field
 * @param {*} value - value of field
 * @returns {*}  - error message or null
 */
export default function validateField(nameFiled, value) {
  switch (nameFiled) {
    case EMAIL:
      return validateEmail(value);
    case PRICE:
      return validateNumber(value);
    case PASSWORD:
      return validatePassword(value);
    case VAIDATE_LENGTH:
      return validateLength(value);
    default:
      return validateString(nameFiled, value);
  }
}
