import {
  CREATE_USER_FAIL,
  CREATE_USER_PENDING,
  CREATE_USER_SUCCESS,
  MODIFY_USER_FAIL,
  MODIFY_USER_PENDING,
  MODIFY_USER_SUCCESS,
  REMOVE_USER_FAIL,
  REMOVE_USER_PENDING,
  REMOVE_USER_SUCCESS,
  GET_USERS_FAIL,
  GET_USERS_PENDING,
  GET_USERS_SUCCESS,
  GET_USER_BY_ID_FAIL,
  GET_USER_BY_ID_PENDING,
  GET_USER_BY_ID_SUCCESS
} from '../actions';

/**
 * Create of pending action
 * @function createUserPending
 * @returns {Object} - New action
 */
export function createUserPending() {
  return {
    type: CREATE_USER_PENDING
  };
}

/**
 * Create of add user action
 * @function createUserSuccess
 * @param {Object} payload - Data of the user
 * @returns {Object} - New action
 */
export function createUserSuccess(payload) {
  return {
    type: CREATE_USER_SUCCESS,
    payload
  };
}

/**
 * Create of error action
 * @function createUserFail
 * @param {Object} error - Error details
 * @returns {Object} - New action
 */
export function createUserFail(error) {
  return {
    type: CREATE_USER_FAIL,
    error
  };
}

/**
 * Create of pending action
 * @function modifyUserPending
 * @returns {Object} - New action
 */
export function modifyUserPending() {
  return {
    type: MODIFY_USER_PENDING
  };
}

/**
 * Create of edit user action
 * @function modifyUserSuccess
 * @param {Object} payload - Data of the user
 * @returns {Object} - New action
 */
export function modifyUserSuccess(payload) {
  return {
    type: MODIFY_USER_SUCCESS,
    payload
  };
}

/**
 * Create of error action
 * @function modifyUserFail
 * @param {Object} error - Error details
 * @returns {Object} - New action
 */
export function modifyUserFail(error) {
  return {
    type: MODIFY_USER_FAIL,
    payload: error
  };
}

/**
 * Create of pending action
 * @function removeUserPending
 * @returns {Object} - New action
 */
export function removeUserPending() {
  return {
    type: REMOVE_USER_PENDING
  };
}

/**
 * Create of remove action
 * @function removeUserSuccess
 * @param {String} id - unique id of item
 * @param {String} status - status of query
 * @returns {Object} - New action
 */
export function removeUserSuccess(id, status) {
  return {
    type: REMOVE_USER_SUCCESS,
    payload: {
      id,
      status
    }
  };
}

/**
 * Create of error action
 * @function removeUserFail
 * @param {Object} error - Error details
 * @returns {Object} - New action
 */
export function removeUserFail(error) {
  return {
    type: REMOVE_USER_FAIL,
    payload: error
  };
}

/**
 * Create of pending action
 * @function getUsersPending
 * @returns {Object} - New action
 */
export function getUsersPending() {
  return {
    type: GET_USERS_PENDING
  };
}

/**
 * Create of user action
 * @function getUsersSuccess
 * @param {Array} users - Data of users
 * @returns {Object} - New action
 */
export function getUsersSuccess(users) {
  return {
    type: GET_USERS_SUCCESS,
    users
  };
}

/**
 * Create of error action
 * @function getUsersFail
 * @param {Object} error - Error details
 * @returns {Object} - New action
 */
export function getUsersFail(error) {
  return {
    type: GET_USERS_FAIL,
    error
  };
}

/**
 * Create of pending action
 * @function getUserByIdPending
 * @returns {Object} - New action
 */
export function getUserByIdPending() {
  return {
    type: GET_USER_BY_ID_PENDING
  };
}

/**
 * Create of user action
 * @function getUserByIdSuccess
 * @param {Object} user - Data of users
 * @returns {Object} - New action
 */
export function getUserByIdSuccess(user) {
  return {
    type: GET_USER_BY_ID_SUCCESS,
    user
  };
}

/**
 * Create of error action
 * @function getUserByIdFail
 * @param {Object} error - Error details
 * @returns {Object} - New action
 */
export function getUserByIdFail(error) {
  return {
    type: GET_USER_BY_ID_FAIL,
    error
  };
}
