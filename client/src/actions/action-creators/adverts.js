import {
  CREATE_ADVERT_FAIL,
  CREATE_ADVERT_PENDING,
  CREATE_ADVERT_SUCCESS,
  MODIFY_ADVERT_FAIL,
  MODIFY_ADVERT_PENDING,
  MODIFY_ADVERT_SUCCESS,
  REMOVE_ADVERT_FAIL,
  REMOVE_ADVERT_PENDING,
  REMOVE_ADVERT_SUCCESS,
  GET_ADVERTS_FAIL,
  GET_ADVERTS_PENDING,
  GET_ADVERTS_SUCCESS,
  GET_ADVERT_BY_ID_FAIL,
  GET_ADVERT_BY_ID_PENDING,
  GET_ADVERT_BY_ID_SUCCESS
} from '../actions';


/**
 * Create of pending action
 * @function createAdvertPending
 * @returns {Object} - New action
 */
export function createAdvertPending() {
  return {
    type: CREATE_ADVERT_PENDING
  };
}

/**
 * Create of add advert action
 * @function createAdvertSuccess
 * @param {Object} status - Data of the advert
 * @returns {Object} - New action
 */
export function createAdvertSuccess(status) {
  return {
    type: CREATE_ADVERT_SUCCESS,
    payload: status
  };
}

/**
 * Create of error action
 * @function createAdvertFail
 * @param {Object} error - Error details
 * @returns {Object} - New action
 */
export function createAdvertFail(error) {
  return {
    type: CREATE_ADVERT_FAIL,
    payload: error
  };
}

/**
 * Create of pending action
 * @function modifyAdvertPending
 * @returns {Object} - New action
 */
export function modifyAdvertPending() {
  return {
    type: MODIFY_ADVERT_PENDING
  };
}

/**
 * Create of edit advert action
 * @function modifyAdvertSuccess
 * @param {Object} status - status of the query
 * @returns {Object} - New action
 */
export function modifyAdvertSuccess(status) {
  return {
    type: MODIFY_ADVERT_SUCCESS,
    payload: status
  };
}

/**
 * Create of error action
 * @function modifyAdvertFail
 * @param {Object} error - Error details
 * @returns {Object} - New action
 */
export function modifyAdvertFail(error) {
  return {
    type: MODIFY_ADVERT_FAIL,
    payload: error
  };
}

/**
 * Create of pending action
 * @function removeAdvertPending
 * @returns {Object} - New action
 */
export function removeAdvertPending() {
  return {
    type: REMOVE_ADVERT_PENDING
  };
}

/**
 * Create of remove action
 * @function removeAdvertSuccess
 * @param {String} id - unique id of item
 * @param {String} status - status of query
 * @returns {Object} - New action
 */
export function removeAdvertSuccess(id, status) {
  return {
    type: REMOVE_ADVERT_SUCCESS,
    payload: {
      id,
      status
    }
  };
}

/**
 * Create of error action
 * @function removeAdvertFail
 * @param {Object} error - Error details
 * @returns {Object} - New action
 */
export function removeAdvertFail(error) {
  return {
    type: REMOVE_ADVERT_FAIL,
    payload: error
  };
}

/**
 * Create of pending action
 * @function getAdvertsPending
 * @returns {Object} - New action
 */
export function getAdvertsPending() {
  return {
    type: GET_ADVERTS_PENDING
  };
}

/**
 * Create of get adverts action
 * @function getAdvertsSuccess
 * @param {Array} adverts - Data of adverts
 * @returns {Object} - New action
 */
export function getAdvertsSuccess(adverts) {
  return {
    type: GET_ADVERTS_SUCCESS,
    adverts
  };
}

/**
 * Create of error action
 * @function getAdvertsFail
 * @param {Object} error - Error details
 * @returns {Object} - New action
 */
export function getAdvertsFail(error) {
  return {
    type: GET_ADVERTS_FAIL,
    error
  };
}

/**
 * Create of pending action
 * @function getAdvertByIdPending
 * @returns {Object} - New action
 */
export function getAdvertByIdPending() {
  return {
    type: GET_ADVERT_BY_ID_PENDING
  };
}

/**
 * Create of get advert action
 * @function getAdvertByIdSuccess
 * @param {Array} advert - Data of advert
 * @returns {Object} - New action
 */
export function getAdvertByIdSuccess(advert) {
  return {
    type: GET_ADVERT_BY_ID_SUCCESS,
    advert
  };
}

/**
 * Create of error action
 * @function getAdvertByIdFail
 * @param {Object} error - Error details
 * @returns {Object} - New action
 */
export function getAdvertsByIdFail(error) {
  return {
    type: GET_ADVERT_BY_ID_FAIL,
    error
  };
}
