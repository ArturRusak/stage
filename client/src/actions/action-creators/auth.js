import {
  LOGOUT,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGIN_PENDING
} from '../actions';

/**
 * Create of pending action
 * @function logOut
 * @returns {Object} - New action
 */
export function logOutAction() {
  return {
    type: LOGOUT,
  };
}

/**
 * Create of pending action
 * @function loginPending
 * @returns {Object} - New action
 */
export function loginPending() {
  return {
    type: LOGIN_PENDING
  };
}

/**
 * Create of login user action
 * @function login
 * @param {Object} payload - credentials of the user
 * @returns {Object} - New action
 */
export function login(payload) {
  return {
    type: LOGIN_SUCCESS,
    payload
  };
}

/**
 * Create of error action
 * @function loginFail
 * @param {Object} payload - Error details
 * @returns {Object} - New action
 */
export function loginFail(payload) {
  return {
    type: LOGIN_FAIL,
    payload
  };
}
