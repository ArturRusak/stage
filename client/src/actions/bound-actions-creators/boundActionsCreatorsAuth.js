import {
  loginFail,
  loginPending,
  login,
  logOutAction
} from '../action-creators/auth';
import loginApi from '../../services/api/auth';
import setAuthorizationToken from '../../services/setAuthorizationToken';


export function logOut() {
  return (dispatch) => {
    localStorage.removeItem('token');
    setAuthorizationToken(false);
    dispatch(logOutAction());
  };
}

/**
 * Get all items Query
 * @function loginQuery
 * @param {Object} dataUser - input values
 * @returns {Function} - dispatch function
 */
export function loginQuery(dataUser) { // TODO FIX RENAME CREDENTIALS
  return (dispatch) => {
    dispatch(loginPending());
    loginApi(dataUser)
      .then((response) => {
        const { token } = response.data;
        localStorage.setItem('token', token);
        setAuthorizationToken(token);
        return response.data;
      })
      .then(data => dispatch(login(data.token)))
      .catch(error => dispatch(loginFail(error)));
  };
}
