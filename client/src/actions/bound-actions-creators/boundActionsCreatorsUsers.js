import {
  createUserFail,
  createUserPending,
  createUserSuccess,
  modifyUserFail,
  modifyUserPending,
  modifyUserSuccess,
  removeUserFail,
  removeUserPending,
  removeUserSuccess,
  getUsersFail,
  getUsersPending,
  getUsersSuccess,
  getUserByIdFail,
  getUserByIdPending,
  getUserByIdSuccess
} from '../action-creators/users';
import {
  getUsersApi,
  getUserByIdApi,
  removeUserApi,
  createUserApi,
  modifyUserApi
} from '../../services/api/users';

/**
 * Get all items Query
 * @function getAllUsersQuery
 * @returns {Function} - dispatch function
 */
export function getAllUsersQuery() {
  return (dispatch) => {
    dispatch(getUsersPending());
    getUsersApi()
      .then(response => response.data.users)
      .then(items => dispatch(getUsersSuccess(items)))
      .catch(error => dispatch(getUsersFail(error)));
  };
}

/**
 * Get items Query
 * @function getAllUsersQuery
 * @param {String} id - Unique id of item
 * @returns {Function} - dispatch function
 */
export function getUserByIdQuery(id) {
  return (dispatch) => {
    dispatch(getUserByIdPending());
    getUserByIdApi(id)
      .then(response => response.data.user)
      .then(item => dispatch(getUserByIdSuccess(item)))
      .catch(error => dispatch(getUserByIdFail(error)));
  };
}


/**
 * Remove query User by unique id
 * @function removeUserQuery
 * @param {String} id - Unique id of item
 * @returns {Function} - dispatch function
 */
export function removeUserQuery(id) {
  return (dispatch) => {
    dispatch(removeUserPending());
    removeUserApi(id)
      .then(response => response.data.status)
      .then(status => dispatch(removeUserSuccess(id, status)))
      .catch(error => dispatch(removeUserFail(error)));
  };
}

/**
 * Add data item Query
 * @function createUserQuery
 * @param {Object} item - Data of the item
 * @returns {Function} - dispatch function
 */
export function createUserQuery(item) {
  return (dispatch) => {
    dispatch(createUserPending());
    createUserApi(item)
      .then(response => (response.data.status))
      .then(data => dispatch(createUserSuccess(data)))
      .catch(error => dispatch(createUserFail(error)));
  };
}

/**
 * Edit data item Query
 * @function modifyUserQuery
 * @param {Object} item - Data of the item
 * @returns {Function} - dispatch function
 */
export function modifyUserQuery(item) {
  return (dispatch) => {
    dispatch(modifyUserPending());
    modifyUserApi(item)
      .then(response => (response.data.status))
      .then(data => dispatch(modifyUserSuccess(data)))
      .catch(error => dispatch(modifyUserFail(error)));
  };
}
