import {
  createAdvertFail,
  createAdvertPending,
  createAdvertSuccess,
  modifyAdvertFail,
  modifyAdvertPending,
  modifyAdvertSuccess,
  removeAdvertFail,
  removeAdvertPending,
  removeAdvertSuccess,
  getAdvertsFail,
  getAdvertsPending,
  getAdvertsSuccess,
  getAdvertsByIdFail,
  getAdvertByIdPending,
  getAdvertByIdSuccess
} from '../action-creators/adverts';
import {
  getAdvertsApi,
  getAdvertByIdApi,
  removeAdvertApi,
  createAdvertApi,
  modifyAdvertApi,
  updateAdvertViewCounterApi
} from '../../services/api/adverts';

/**
 * Add data item Query
 * @function createAdvertQuery
 * @param {Object} item - Data of the item
 * @returns {Function} - dispatch function
 */
export function createAdvertQuery(item) {
  return (dispatch) => {
    dispatch(createAdvertPending());
    createAdvertApi(item)
      .then(response => (response.data.status))
      .then(advert => dispatch(createAdvertSuccess(advert)))
      .catch(error => dispatch(createAdvertFail(error)));
  };
}

/**
 * Edit data item Query
 * @function modifyAdvertQuery
 * @param {Object} item - Data of the item
 * @returns {Function} - dispatch function
 */
export function modifyAdvertQuery(item) {
  return (dispatch) => {
    dispatch(modifyAdvertPending());
    modifyAdvertApi(item)
      .then(response => (response.data.status))
      .then(advert => dispatch(modifyAdvertSuccess(advert)))
      .catch(error => dispatch(modifyAdvertFail(error)));
  };
}

/**
 * Get all items Query
 * @function getAllAdvertsQuery
 * @returns {Function} - dispatch function
 */
export function getAllAdvertsQuery() {
  return (dispatch) => {
    dispatch(getAdvertsPending());
    getAdvertsApi()
      .then(response => response.data.adverts)
      .then(items => dispatch(getAdvertsSuccess(items)))
      .catch(error => dispatch(getAdvertsFail(error)));
  };
}

/**
 * Get item Query
 * @function getAdvertByIdQuery
 * @param {String} id - id of item
 * @returns {Function} - dispatch function
 */
export function getAdvertByIdQuery(id) {
  return (dispatch) => {
    dispatch(getAdvertByIdPending());
    getAdvertByIdApi(id)
      .then(response => response.data.advert)
      .then(advert => dispatch(getAdvertByIdSuccess(advert)))
      .then(() => updateAdvertViewCounterApi(id))
      .catch(error => dispatch(getAdvertsByIdFail(error)));
  };
}

/**
 * Remove data by unique id Query
 * @function removeAdvertQuery
 * @param {String} id - Unique id of item
 * @returns {Function} - dispatch function
 */
export function removeAdvertQuery(id) {
  return (dispatch) => {
    dispatch(removeAdvertPending());
    removeAdvertApi(id)
      .then(response => response.data.status)
      .then(status => dispatch(removeAdvertSuccess(id, status)))
      .catch(error => dispatch(removeAdvertFail(error)));
  };
}
