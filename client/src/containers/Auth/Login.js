import React, { Component } from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loginQuery } from '../../actions/bound-actions-creators/boundActionsCreatorsAuth';

import ServerError from '../Adverts/CreateAndEditAdvert';
import Spinner from '../Adverts/Adverts';
import LoginForm from '../../components/Auth/LoginForm';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthenticated: false
    };
  }

  componentDidUpdate() {
    const { isAuthenticated } = this.props;
    if (isAuthenticated) {
      this.setState({ isAuthenticated: true });
    }
  }

  render() {
    const { isAuthenticated } = this.state;
    const { error, loading, login } = this.props;
    return (
      <div className="container">
        <div className="container-wrapper">
          {loading && <Spinner />}
          {error && (
            <ServerError
              error={error}
            />
          )}
          {isAuthenticated
            ? (<Redirect to="/" />) : (<LoginForm login={login} />)
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { auth } = state;
  return {
    isAuthenticated: auth.isAuthenticated,
    error: auth.error,
    loading: auth.loading
  };
};

const mapDispatchToProps = dispatch => ({
  login: inputData => dispatch(loginQuery(inputData))
});

Login.defaultProps = {
  error: null,
};

Login.propTypes = {
  login: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
