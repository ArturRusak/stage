import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { NavLink, withRouter } from 'react-router-dom';
import { USERS, ADVERTS, LOGIN } from '../../constants';
import { logOut } from '../../actions/bound-actions-creators/boundActionsCreatorsAuth';
import './Navigation.css';

/**
 * Represents navigation menu
 * @constructor
 * @returns {String} - Stateless component
 */

class Navigation extends Component {
  logout(event) {
    const { logOut } = this.props;
    event.preventDefault();
    logOut();
  }

  render() {
    const { isAuthenticated } = this.props;

    const userLinks = (
      <ul className="nav">
        <li>
          <button
            type="button"
            onClick={this.logout.bind(this)}
          >
            Logout
          </button>
        </li>
      </ul>
    );

    const guestLinks = (
      <ul className="nav">
        <li><NavLink to="/registration">Sign up</NavLink></li>
        <li><NavLink to="/login">{LOGIN}</NavLink></li>
      </ul>
    );

    return (
      <nav className="navbar">
        { isAuthenticated ? userLinks : guestLinks }
        <ul className="menu">
          <li className="menu__item">
            <NavLink activeClassName="active" to="/users">{USERS}</NavLink>
          </li>
          <li className="menu__item">
            <NavLink activeClassName="active" to="/adverts">{ADVERTS}</NavLink>
          </li>
        </ul>
      </nav>
    );
  }
}

Navigation.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  logOut: PropTypes.func.isRequired
};

const mapStateToProps = state => state.auth;

export default withRouter(connect(mapStateToProps, { logOut })(Navigation));
