import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as advertActions from '../../actions/bound-actions-creators/boundActionsCreatorsAdverts';
import AdvertInfo from '../../components/Advert/AdvertInfo';
import UserContacts from '../../components/User/UserContacts';
import Spinner from '../../components/Spinner/Spinner';
import ServerError from '../../components/ServerError/ServerError';
import { ADVERT_DETAILS } from '../../constants';

/**
 * Users page
 */
class AdvertDetails extends Component {
  /**
   * Prop types
   * @param {Object} props - Property of instance
   */
  constructor(props) {
    super(props);

    this.handleRemoveAdvert = this.handleRemoveAdvert.bind(this);
  }

  /**
   * @returns {void} getAdvertById - Get adverts from data
   */
  componentDidMount() {
    const { getAdvertById, match } = this.props;
    getAdvertById(match.params.id);
  }

  /**
   * Remove item
   * @param {Number} id - unique of item
   * @returns {void}
   */
  handleRemoveAdvert(id) {
    const { removeAdvertById } = this.props;
    removeAdvertById(id);
  }

  /**
   * Represents the adverts table, buttons
   * @returns {XML} - Users page
   */
  render() {
    const {
      advert, loading, error
    } = this.props;
    return (
      <div className="container">
        {loading && <Spinner />}
        {!loading && (
          <div className="container-wrapper">
            <h1>{ADVERT_DETAILS}</h1>
            {error && (
              <ServerError
                error={error}
              />
            )}
            <AdvertInfo
              advertData={advert}
              onAdvertRemove={this.handleRemoveAdvert}
            />
            <UserContacts
              userData={advert}
            />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { adverts } = state;
  return {
    advert: adverts.advert.data,
    loading: adverts.advert.loading,
    error: adverts.advert.error
  };
};

const mapDispatchToProps = dispatch => ({
  getAdvertById: id => dispatch(advertActions.getAdvertByIdQuery(id)),
  removeAdvertById: id => dispatch(advertActions.removeAdvertQuery(id))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AdvertDetails));

AdvertDetails.defaultProps = {
  error: null,
  match: null
};

AdvertDetails.propTypes = {
  advert: PropTypes.arrayOf(PropTypes.object).isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string,
  match: PropTypes.shape,
  getAdvertById: PropTypes.func.isRequired,
  removeAdvertById: PropTypes.func.isRequired
};
