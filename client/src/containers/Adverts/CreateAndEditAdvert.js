import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as advertActions from '../../actions/bound-actions-creators/boundActionsCreatorsAdverts';
import * as userActions from '../../actions/bound-actions-creators/boundActionsCreatorsUsers';
import AdvertForm from '../../components/Advert/AdvertForm';
import Spinner from '../../components/Spinner/Spinner';
import ServerError from '../../components/ServerError/ServerError';
import {
  ADD_NEW_ADVERT,
  EDIT_ADVERT_TITLE
} from '../../constants';

/**
 * Users page
 */
class CreateAndEditAdvert extends Component {
  /**
   * Prop types
   * @param {Object} props - Property of instance
   */
  constructor(props) {
    super(props);
    const { match, adverts } = this.props;
    let targetAdvert = null;
    if (match.params.id) {
      if (match.params.id) {
        const [advertObj] = adverts.filter(item => item.id.toString() === match.params.id);
        targetAdvert = advertObj;
      }
    }
    this.state = {
      advert: targetAdvert ? { ...targetAdvert } : targetAdvert
    };

    this.handleCreateAdvert = this.handleCreateAdvert.bind(this);
    this.handleModifyAdvert = this.handleModifyAdvert.bind(this);
  }

  /**
   * @returns {void} getAdverts - Get adverts from data
   */
  componentDidMount() {
    const { getAllUsers } = this.props;
    getAllUsers();
  }

  /**
   * Create new advert
   * @param {Object} item - data of new advert
   * @returns {*} - Call to data
   */
  handleCreateAdvert(item) {
    const { createAdvert } = this.props;
    createAdvert(item);
  }

  /**
   * Modify advert
   * @param {Object} item - data of new advert
   * @returns {*} - Call to data
   */
  handleModifyAdvert(item) {
    const { modifyAdvert } = this.props;
    modifyAdvert(item);
  }

  /**
   * Represents the advert form
   * @returns {*} - Adverts form
   */
  render() {
    const {
      error, loading, users,
      modifiedAdvert, createdAdvert, match
    } = this.props;
    const { advert } = this.state;
    const isEdit = !!match.params.id;

    return (
      <div className="container">
        {loading && <Spinner />}
        <div className="container-wrapper">
          <h1>{isEdit ? EDIT_ADVERT_TITLE : ADD_NEW_ADVERT}</h1>
          {error && (
            <ServerError
              error={error}
            />
          )}
          <AdvertForm
            advert={advert}
            users={users}
            isEditForm={isEdit}
            modifiedAdvert={modifiedAdvert}
            createdAdvert={createdAdvert}
            onCreateAdvert={this.handleCreateAdvert}
            onModifyAdvert={this.handleModifyAdvert}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { users, adverts } = state;
  return {
    users: users.users.data,
    adverts: adverts.adverts.data,
    loading: adverts.createdAdvert.loading || adverts.modifiedAdvert.loading,
    error: adverts.createdAdvert.error || adverts.modifiedAdvert.error,
    createdAdvert: adverts.createdAdvert,
    modifiedAdvert: adverts.modifiedAdvert
  };
};

const mapDispatchToProps = dispatch => ({
  createAdvert: item => dispatch(advertActions.createAdvertQuery(item)),
  modifyAdvert: obj => dispatch(advertActions.modifyAdvertQuery(obj)),
  getAllUsers: obj => dispatch(userActions.getAllUsersQuery(obj)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateAndEditAdvert));

CreateAndEditAdvert.defaultProps = {
  error: null,
  match: null,
  adverts: [],
  users: []
};

CreateAndEditAdvert.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string,
  createdAdvert: PropTypes.shape({
    status: PropTypes.string,
    error: PropTypes.string,
    loading: PropTypes.bool
  }).isRequired,
  modifiedAdvert: PropTypes.shape({
    status: PropTypes.string,
    error: PropTypes.string,
    loading: PropTypes.bool
  }).isRequired,
  users: PropTypes.arrayOf(PropTypes.object),
  adverts: PropTypes.arrayOf(PropTypes.object),
  match: PropTypes.shape(PropTypes.objectOf),
  createAdvert: PropTypes.func.isRequired,
  modifyAdvert: PropTypes.func.isRequired,
  getAllUsers: PropTypes.func.isRequired
};
