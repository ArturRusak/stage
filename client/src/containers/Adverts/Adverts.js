import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import * as advertActions from '../../actions/bound-actions-creators/boundActionsCreatorsAdverts';
import AdvertsTable from '../../components/Advert/AdvertsTable';
import Spinner from '../../components/Spinner/Spinner';
import ServerError from '../../components/ServerError/ServerError';
import Message from '../../components/Common/Message';
import {
  ALL_ADVERTS,
  ADD_ADVERT
} from '../../constants';

/**
 * adverts page
 */
class Adverts extends Component {
  /**
   * Prop types
   * @param {Object} props - Property of instance
   */
  constructor(props) {
    super(props);
    this.state = {
      pushed: false
    };
    this.handleRemoveAdvert = this.handleRemoveAdvert.bind(this);
  }

  /**
   * @returns {void} getAdverts - Get adverts from data
   */
  componentDidMount() {
    const { getAdverts } = this.props;
    getAdverts();
  }

  /**
   * Remove advert from table
   * @param {Number} id - get id of item
   * @returns {void}
   */
  handleRemoveAdvert(id) {
    const { removeAdvert } = this.props;
    removeAdvert(id);
    const { pushed } = this.state;
    this.setState({
      pushed: !pushed
    });
  }

  /**
   * * Represents the advert table, buttons, modal windows
   */
  render() {
    const {
      adverts, loading, error, successQueryStatus
    } = this.props;
    const { pushed } = this.state;

    return (
      <div className="container">
        {loading && <Spinner />}
        <div className="container-wrapper">
          <h1>{ALL_ADVERTS}</h1>
          {error && (
          <ServerError
            error={error}
          />
          )}
          <Link
            className="action-btn"
            to="/new-advert/"
          >
            {ADD_ADVERT}
          </Link>
          {successQueryStatus && pushed ? (
            <Message
              className="success-message"
              content={successQueryStatus}
            />
          ) : null}
          <AdvertsTable
            adverts={adverts}
            onAdvertRemove={this.handleRemoveAdvert}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { adverts } = state;
  return {
    adverts: adverts.adverts.data,
    loading: adverts.removedAdvert.loading || adverts.adverts.loading,
    error: adverts.removedAdvert.error || adverts.adverts.error,
    successQueryStatus: adverts.removedAdvert.status
  };
};

const mapDispatchToProps = dispatch => ({
  getAdverts: () => dispatch(advertActions.getAllAdvertsQuery()),
  removeAdvert: id => dispatch(advertActions.removeAdvertQuery(id))
});

Adverts.defaultProps = {
  error: null,
  successQueryStatus: null,
  adverts: []
};

Adverts.propTypes = {
  error: PropTypes.string,
  successQueryStatus: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  adverts: PropTypes.arrayOf(PropTypes.object),
  getAdverts: PropTypes.func.isRequired,
  removeAdvert: PropTypes.func.isRequired
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Adverts));
