import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import * as userActions from '../../actions/bound-actions-creators/boundActionsCreatorsUsers';
import * as advertActions from '../../actions/bound-actions-creators/boundActionsCreatorsAdverts';
import AdvertInfo from '../../components/Advert/AdvertInfo';
import UserInfo from '../../components/User/UserInfo';
import Spinner from '../../components/Spinner/Spinner';
import ServerError from '../../components/ServerError/ServerError';
import {
  USER_DETAILS,
  USER_ADVERTS,
  ADD_ADVERT
} from '../../constants';

/**
 * Users page
 */
class UserDetails extends Component {
  /**
   * Prop types
   * @param {Object} props - Property of instance
   */
  constructor(props) {
    super(props);

    this.handleRemoveAdvert = this.handleRemoveAdvert.bind(this);
  }

  /**
   * @returns {void} getuser - Get user from data
   */
  componentDidMount() {
    const { getUserById, match } = this.props;
    getUserById(match.params.id);
  }

  /**
   * Remove item
   * @param {Number} id - unique of item
   * @returns {void}
   */
  handleRemoveAdvert(id) {
    const { removeAdvertById } = this.props;
    removeAdvertById(id);
  }

  /**
   * Represents the users table, buttons
   * @returns {XML} - Adverts page
   */
  render() {
    const {
      user, loading, error
    } = this.props;
    return (
      <div className="container">
        {loading && <Spinner />}
        <div className="container-wrapper">
          <h1>{USER_DETAILS}</h1>
          {error && (
            <ServerError
              error={error}
            />
          )}
          <Link
            to="/new-advert"
            className="action-btn"
          >
            {ADD_ADVERT}
          </Link>
          <UserInfo
            user={user}
          />
          <div className="additional-info">
            <h2>{USER_ADVERTS}</h2>
            <AdvertInfo
              advertData={user}
              onAdvertRemove={this.handleRemoveAdvert}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { users } = state;
  return {
    user: users.user.data,
    loading: users.user.loading,
    error: users.user.error
  };
};

const mapDispatchToProps = dispatch => ({
  getUserById: id => dispatch(userActions.getUserByIdQuery(id)),
  removeAdvertById: id => dispatch(advertActions.removeAdvertQuery(id))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UserDetails));

UserDetails.defaultProps = {
  error: null,
  match: null
};

UserDetails.propTypes = {
  user: PropTypes.arrayOf(PropTypes.object).isRequired,
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string,
  match: PropTypes.shape,
  getUserById: PropTypes.func.isRequired,
  removeAdvertById: PropTypes.func.isRequired
};
