import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as userActions from '../../actions/bound-actions-creators/boundActionsCreatorsUsers';
import UserForm from '../../components/User/UserForm';
import Spinner from '../../components/Spinner/Spinner';
import ServerError from '../../components/ServerError/ServerError';
import {
  CREATE_NEW_USER,
  EDIT_TARGET_USER,
} from '../../constants';

/**
 * Users page
 */
class CreateAndEditUser extends Component {
  /**
   * Prop types
   * @param {Object} props - Property of instance
   */
  constructor(props) {
    super(props);
    const { match, users } = this.props;
    let targetUser = null;
    if (match.params.id) {
      const [userObj] = users.filter(item => item.id.toString() === match.params.id);
      targetUser = userObj;
    }
    this.state = {
      user: targetUser ? { ...targetUser } : targetUser
    };
    this.handleCreateUser = this.handleCreateUser.bind(this);
    this.handleModifyUser = this.handleModifyUser.bind(this);
  }

  /**
   * Create new User
   * @param {Object} item - data of new user
   * @returns {*} - Call to data
   */
  handleCreateUser(item) {
    const { createUser } = this.props;
    createUser(item);
  }

  /**
   * Modify User
   * @param {Object} item - data of new user
   * @returns {*} - Call to data
   */
  handleModifyUser(item) {
    const { modifyUser } = this.props;
    modifyUser(item);
  }

  /**
   * Represents the users table, buttons
   * @returns {XML} - Adverts page
   */
  render() {
    const {
      error, loading,
      createdUser, modifiedUser,
      match
    } = this.props;
    const { user } = this.state;
    const isEdit = !!match.params.id;

    return (
      <div className="container">
        {loading && <Spinner />}
        <div className="container-wrapper">
          <h1>{isEdit ? EDIT_TARGET_USER : CREATE_NEW_USER}</h1>
          {error && (
            <ServerError
              error={error}
            />
          )}
          <UserForm
            user={user}
            isEditForm={isEdit}
            createdUser={createdUser}
            modifiedUser={modifiedUser}
            onCreateUser={this.handleCreateUser}
            onModifyUser={this.handleModifyUser}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { users } = state;
  return {
    users: users.users.data,
    loading: users.createdUser.loading || users.modifiedUser.loading,
    error: users.createdUser.error || users.modifiedUser.error,
    createdUser: users.createdUser,
    modifiedUser: users.modifiedUser
  };
};

const mapDispatchToProps = dispatch => ({
  createUser: item => dispatch(userActions.createUserQuery(item)),
  modifyUser: obj => dispatch(userActions.modifyUserQuery(obj)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreateAndEditUser));

CreateAndEditUser.defaultProps = {
  error: null,
  match: null,
  users: []
};

CreateAndEditUser.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: PropTypes.string,
  createdUser: PropTypes.shape({
    status: PropTypes.string,
    error: PropTypes.string,
    loading: PropTypes.bool
  }).isRequired,
  modifiedUser: PropTypes.shape({
    status: PropTypes.string,
    error: PropTypes.string,
    loading: PropTypes.bool
  }).isRequired,
  users: PropTypes.arrayOf(PropTypes.object),
  match: PropTypes.shape(PropTypes.objectOf),
  createUser: PropTypes.func.isRequired,
  modifyUser: PropTypes.func.isRequired
};
