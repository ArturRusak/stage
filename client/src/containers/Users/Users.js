import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import * as userActions from '../../actions/bound-actions-creators/boundActionsCreatorsUsers';
import UsersTable from '../../components/User/UsersTable';
import Spinner from '../../components/Spinner/Spinner';
import ServerError from '../../components/ServerError/ServerError';
import Message from '../../components/Common/Message';
import { CREATE_NEW_USER, ALL_USERS } from '../../constants';

/**
 * Users page
 */
class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pushed: false
    };
    this.handleRemoveUser = this.handleRemoveUser.bind(this);
  }

  componentDidMount() {
    const { getUsers } = this.props;
    getUsers();
  }

  /**
   * Remove user from table
   * @param {Number} id - get id of item
   * @returns {void}
   */
  handleRemoveUser(id) {
    const { removeUser } = this.props;
    removeUser(id);
    const { pushed } = this.state;
    this.setState({
      pushed: !pushed
    });
  }

  /**
   * Represents the user table, buttons, modal windows
   * @returns {*} - Users page
   */
  render() {
    const {
      users, loading, error, successQueryStatus
    } = this.props;
    const { pushed } = this.state;

    return (
      <div className="container">
        {loading && <Spinner />}
        <div className="container-wrapper">
          <h1>{ALL_USERS}</h1>
          {error && (
          <ServerError
            error={error}
          />
          )}
          <Link
            to="/new-user"
            className="action-btn"
          >
            {CREATE_NEW_USER}
          </Link>
          {successQueryStatus && pushed ? (
            <Message
              className="success-message"
              content={successQueryStatus}
            />
          ) : null}
          <UsersTable
            users={users}
            onUserRemove={this.handleRemoveUser}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { users } = state;
  return {
    users: users.users.data,
    loading: users.removedUser.loading || users.users.loading,
    error: users.removedUser.error || users.users.error,
    successQueryStatus: users.removedUser.status
  };
};

const mapDispatchToProps = dispatch => ({
  getUsers: () => dispatch(userActions.getAllUsersQuery()),
  removeUser: id => dispatch(userActions.removeUserQuery(id))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Users));

Users.defaultProps = {
  error: null,
  successQueryStatus: null,
  users: []
};

Users.propTypes = {
  error: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  successQueryStatus: PropTypes.string,
  users: PropTypes.arrayOf(PropTypes.object),
  getUsers: PropTypes.func.isRequired,
  removeUser: PropTypes.func.isRequired
};
