import {
  CREATE_USER_FAIL,
  CREATE_USER_PENDING,
  CREATE_USER_SUCCESS,
  MODIFY_USER_FAIL,
  MODIFY_USER_PENDING,
  MODIFY_USER_SUCCESS,
  REMOVE_USER_FAIL,
  REMOVE_USER_PENDING,
  REMOVE_USER_SUCCESS,
  GET_USERS_FAIL,
  GET_USERS_SUCCESS,
  GET_USERS_PENDING,
  GET_USER_BY_ID_FAIL,
  GET_USER_BY_ID_SUCCESS,
  GET_USER_BY_ID_PENDING,
  REMOVE_ADVERT_SUCCESS
} from '../actions/actions';

const initialState = {
  users: { data: [], loading: false, error: null },
  user: { data: [], loading: false, error: null },
  removedUser: { status: null, loading: false, error: null },
  createdUser: { status: null, loading: false, error: null },
  modifiedUser: { status: null, loading: false, error: null }
};

/**
 * Remove item from state
 * @param {Array} arr - items
 * @param {String} id - unique of item
 * @returns {Array} - New array
 */
function removeItem(arr = [], id) {
  return arr.filter(item => item.id !== id);
}

/**
 * Remove item from state
 * @param {Array} arr - items
 * @param {String} id - unique of item
 * @returns {Array} - New array
 */
function removeAdvert(arr = [], id) {
  if (arr.length === 1) {
    const {
      us_id, userName, firstName, lastName, email, phone
    } = arr[0];
    return [{
      us_id,
      userName,
      firstName,
      lastName,
      email,
      phone
    }];
  }
  return arr.filter(item => item.id !== id);
}

export default (state = initialState, action) => {
  switch (action.type) {
    case CREATE_USER_SUCCESS:
      return {
        ...state,
        createdUser: {
          status: action.payload,
          loading: false,
          error: null
        }
      };
    case MODIFY_USER_SUCCESS:
      return {
        ...state,
        modifiedUser: {
          status: action.payload,
          loading: false,
          error: null
        }
      };
    case REMOVE_USER_SUCCESS:
      return {
        ...state,
        users: {
          ...state.users,
          data: removeItem(state.users.data, action.payload.id)
        },
        removedUser: {
          status: action.payload.status,
          loading: false,
          error: null
        }
      };
    case GET_USERS_SUCCESS:
      return {
        ...state,
        users: {
          data: action.users,
          loading: false,
          error: null
        }
      };
    case REMOVE_ADVERT_SUCCESS:
      return {
        ...state,
        user: {
          data: removeAdvert(state.user.data, action.payload.id),
          loading: false,
          error: null
        }
      };
    case GET_USER_BY_ID_SUCCESS:
      return {
        ...state,
        user: {
          data: action.user,
          loading: false,
          error: null
        }
      };
    case CREATE_USER_PENDING:
      return {
        ...state,
        createdUser: {
          status: null,
          loading: false,
          error: null
        }
      };
    case MODIFY_USER_PENDING:
      return {
        ...state,
        modifiedUser: {
          status: null,
          loading: true,
          error: null
        }
      };
    case REMOVE_USER_PENDING:
      return {
        ...state,
        removedUser: {
          status: null,
          loading: true,
          error: null
        }
      };
    case GET_USERS_PENDING:
      return {
        ...state,
        users: {
          ...state.users,
          loading: true,
          error: null
        }
      };
    case GET_USER_BY_ID_PENDING:
      return {
        ...state,
        user: {
          ...state.user,
          loading: true,
          error: null
        }
      };
    case CREATE_USER_FAIL:
      return {
        ...state,
        createdUser: {
          status: null,
          loading: false,
          error: action.error.message || 'Sorry, error of create user'
        }
      };
    case MODIFY_USER_FAIL:
      return {
        ...state,
        modifiedUser: {
          status: null,
          loading: false,
          error: action.payload.message || 'Sorry, error of modify user'
        }
      };
    case REMOVE_USER_FAIL:
      return {
        ...state,
        removedUser: {
          status: null,
          loading: false,
          error: action.payload.message || 'Sorry, error of removing user'
        }
      };
    case GET_USERS_FAIL:
      return {
        ...state,
        users: {
          ...state.users,
          loading: false,
          error: action.error.message || 'Sorry, error of loading users'
        }
      };
    case GET_USER_BY_ID_FAIL:
      return {
        ...state,
        user: {
          ...state.user,
          loading: false,
          error: action.error.message || 'Sorry, error of loading user by ID'
        }
      };
    default:
      return state;
  }
};
