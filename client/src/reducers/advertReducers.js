import {
  CREATE_ADVERT_FAIL,
  CREATE_ADVERT_PENDING,
  CREATE_ADVERT_SUCCESS,
  MODIFY_ADVERT_FAIL,
  MODIFY_ADVERT_PENDING,
  MODIFY_ADVERT_SUCCESS,
  REMOVE_ADVERT_FAIL,
  REMOVE_ADVERT_PENDING,
  REMOVE_ADVERT_SUCCESS,
  GET_ADVERTS_FAIL,
  GET_ADVERTS_PENDING,
  GET_ADVERTS_SUCCESS,
  GET_ADVERT_BY_ID_FAIL,
  GET_ADVERT_BY_ID_PENDING,
  GET_ADVERT_BY_ID_SUCCESS
} from '../actions/actions';

const initialState = {
  adverts: { data: [], error: null, loading: false },
  advert: { data: [], error: null, loading: false },
  removedAdvert: { status: null, error: null, loading: false },
  createdAdvert: { status: null, error: null, loading: false },
  modifiedAdvert: { status: null, error: null, loading: false }
};

/**
 * Remove item from state
 * @param {Array} arr - items
 * @param {String} id - unique of item
 * @returns {Array} - New array
 */
function removeItem(arr = [], id) {
  return arr.filter(item => item.id !== id);
}

export default (state = initialState, action) => {
  switch (action.type) {
    case CREATE_ADVERT_SUCCESS:
      return {
        ...state,
        createdAdvert: {
          status: action.payload,
          loading: false,
          error: null
        }
      };
    case MODIFY_ADVERT_SUCCESS:
      return {
        ...state,
        modifiedAdvert: {
          status: action.payload,
          loading: false,
          error: null
        }
      };
    case REMOVE_ADVERT_SUCCESS:
      return {
        ...state,
        adverts: {
          ...state.adverts,
          data: removeItem(state.adverts.data, action.payload.id)
        },
        removedAdvert: {
          status: action.payload.status,
          loading: false,
          error: null
        }
      };
    case GET_ADVERTS_SUCCESS:
      return {
        ...state,
        adverts: {
          ...state.adverts,
          data: action.adverts,
          loading: false,
          error: null
        }
      };
    case GET_ADVERT_BY_ID_SUCCESS:
      return {
        ...state,
        advert: {
          data: action.advert,
          loading: false,
          error: null
        }
      };
    case CREATE_ADVERT_PENDING:
      return {
        ...state,
        createdAdvert: {
          status: null,
          loading: true,
          error: null
        }
      };
    case MODIFY_ADVERT_PENDING:
      return {
        ...state,
        modifiedAdvert: {
          status: null,
          loading: true,
          error: null
        }
      };
    case REMOVE_ADVERT_PENDING:
      return {
        ...state,
        removedAdvert: {
          ...state.removedAdvert,
          loading: true,
          error: null
        }
      };
    case GET_ADVERTS_PENDING:
      return {
        ...state,
        adverts: {
          ...state.adverts,
          loading: true,
          error: null
        }
      };
    case GET_ADVERT_BY_ID_PENDING:
      return {
        ...state,
        advert: {
          ...state.advert,
          loading: true,
          error: null
        }
      };
    case CREATE_ADVERT_FAIL:
      return {
        ...state,
        createdAdvert: {
          status: null,
          loading: false,
          error: action.payload.message || 'Sorry, error of create advert'
        }
      };
    case MODIFY_ADVERT_FAIL:
      return {
        ...state,
        modifiedAdvert: {
          status: null,
          loading: false,
          error: action.payload.message || 'Sorry, error of modify advert'
        }
      };
    case REMOVE_ADVERT_FAIL:
      return {
        ...state,
        removedAdvert: {
          status: null,
          loading: false,
          error: action.payload.message || 'Sorry, error of removing advert'
        }
      };
    case GET_ADVERTS_FAIL:
      return {
        ...state,
        adverts: {
          ...state.adverts,
          loading: false,
          error: action.error.message || 'Sorry, error of loading adverts'
        }
      };
    case GET_ADVERT_BY_ID_FAIL:
      return {
        ...state,
        advert: {
          ...state.advert,
          loading: false,
          error: action.error.message || 'Sorry, error of loading advert by ID'
        }
      };
    default:
      return state;
  }
};
