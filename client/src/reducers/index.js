import { combineReducers } from 'redux';
import adverts from './advertReducers';
import auth from './authReducers';
import users from './userReducers';

export default combineReducers({
  adverts,
  users,
  auth
});
