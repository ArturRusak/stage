import jwt from 'jsonwebtoken';
import {
  LOGOUT,
  LOGIN_SUCCESS,
  LOGIN_PENDING,
  LOGIN_FAIL,
} from '../actions/actions';

const initialState = {
  isAuthenticated: false,
  user: {},
  loading: false,
  error: null
};

function decodeToken(token) {
  const user = jwt.decode(token);
  return user.user[0];
}

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        user: decodeToken(action.payload),
        loading: false,
        error: null
      };
    case LOGIN_PENDING:
      return {
        ...state,
        isAuthenticated: false,
        user: {},
        loading: true,
        error: null
      };
    case LOGOUT:
      return {
        ...state,
        isAuthenticated: false,
        user: {},
        loading: false,
        error: null
      };
    case LOGIN_FAIL:
      return {
        ...state,
        isAuthenticated: false,
        user: {},
        loading: false,
        error: action.payload.message || 'Sorry, error of login!'
      };
    default:
      return state;
  }
};
